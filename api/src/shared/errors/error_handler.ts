import APIError = require("./api_error");
import InternalServerError = require("./types/base/internal_server_error");

class ErrorHandler {

    public static handleError(error, request, response, next) {

        let apiError: APIError;

        try {
            apiError = error as APIError;
            response.status(apiError.statusCode)
                .json(apiError);

        } catch (e) {

            apiError = new InternalServerError();
            response.status(apiError.statusCode)
                .json(apiError);
        }

    }
}

export = ErrorHandler;
