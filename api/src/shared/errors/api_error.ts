class APIError extends Error {

    public type: string;
    public subType: string;
    public statusCode: number;
    public reason: string;

    constructor(type: string, subType: string, statusCode: number, reason: string) {
        super(reason);

        this.type = type;
        this.subType = subType;
        this.statusCode = statusCode;
        this.reason = reason;
    }
}

export = APIError;
