import ForbiddenError = require("../base/forbidden_error");

class AuthenticationMissingError extends ForbiddenError {

    constructor() {
        super("authentication_missing", "The necessary authentication credentials were missing from this request");
    }
}

export = AuthenticationMissingError;
