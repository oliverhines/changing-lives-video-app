import UnauthorisedError = require("../base/unauthorised_error");

class InvalidTokenError extends UnauthorisedError {

    constructor() {
        super("invalid_token", "Invalid token supplied");
    }
}

export = InvalidTokenError;
