import UnprocessableEntityError = require("../base/unprocessable_entity_error");

class MissingFieldsError extends UnprocessableEntityError {

    constructor() {
        super("missing_fields", "There was missing fields in the request");
    }
}

export = MissingFieldsError;
