import UnprocessableEntityError = require("../base/unprocessable_entity_error");

class ValidationError extends UnprocessableEntityError {

    public field: string;
    public constraint: string;

    constructor(field: string, constraint: string, message: string) {
        super("validation", message);

        this.field = field;
        this.constraint = constraint;
    }
}

export = ValidationError;
