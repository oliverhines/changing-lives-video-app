import APIError = require("../../api_error");

class UnauthorisedError extends APIError {

    constructor(subType: string, reason: string) {
        super("Unauthorised", subType, 401, reason);
    }
}

export = UnauthorisedError;
