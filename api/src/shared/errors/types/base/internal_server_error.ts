import APIError = require("../../api_error");

class InternalServerError extends APIError {

    constructor() {
        super("Internal Server Error", "internal_server", 500, "An internal server error occurred, please try again later");
    }
}

export = InternalServerError;
