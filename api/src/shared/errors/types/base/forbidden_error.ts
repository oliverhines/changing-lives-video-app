import APIError = require("../../api_error");

class ForbiddenError extends APIError {

    constructor(subType: string, reason: string) {
        super("Forbidden", subType, 403, reason);
    }
}

export = ForbiddenError;
