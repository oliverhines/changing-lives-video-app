import APIError = require("../../api_error");

class UnprocessableEntityError extends APIError {

    constructor(subType: string, reason: string) {
        super("Unprocessable entity", subType, 422, reason);
    }
}

export = UnprocessableEntityError;
