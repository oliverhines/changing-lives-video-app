import APIError = require("../../api_error");

class NotImplementedError extends APIError {

    constructor() {
        super("Not Implemented", "not_implemented", 501, "This endpoint has not yet been implemented.");
    }
}

export = NotImplementedError;
