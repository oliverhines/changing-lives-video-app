import Knex from 'knex';
import Config from "../../config";

class Database {

    private static sharedInstance: Knex;

    // TODO Handle errors better in this file

    public static getSharedInstance(): Knex {

        if (!this.sharedInstance) {

            this.sharedInstance = Knex({
                client: "pg",
                connection: Config.getSharedInstance().getDatabaseConnectionString()
            });
        }

        return this.sharedInstance;
    }

    public static async init() {

        let userTableExists = await this.getSharedInstance().schema.hasTable("cl_users");

        if (!userTableExists) {

            await this.getSharedInstance().schema.createTable("cl_users", table => {

                table.uuid("id").primary();
                table.string("email");
                table.string("hashedPassword");
                table.string("proxyId");
                table.string("privilegeLevel");
            });
        }

        let courseTableExists = await this.getSharedInstance().schema.hasTable("cl_courses");

        if (!courseTableExists) {

            await this.getSharedInstance().schema.createTable("cl_courses", table => {

                table.uuid("id").primary();
                table.string("name");
            });
        }


        let videoTableExists = await this.getSharedInstance().schema.hasTable("cl_videos");

        if (!videoTableExists) {

            await this.getSharedInstance().schema.createTable("cl_videos", table => {

                table.uuid("id").primary();
                table.string("title");
                table.string("courseId");
                table.string("description");
          });
        }
    }
}

export = Database;
