import Database = require("../database");
import InternalServerError = require("../../errors/types/base/internal_server_error");
import Video = require("../../../models/video");
const uuid = require("uuid");

class VideoSelector {

    public static async createNewVideo(courseId: string, title: string, description: string): Promise<Video> {
        return Database.getSharedInstance()("cl_videos")
            .insert({
                id: uuid(),
                courseId: courseId,
                title: title,
                description: description,

            })
            .returning("*")
            .then(([result]) => {

                let video: Video = {
                    id: result.id,
                    courseId: result.courseId,
                    title: result.title,
                    description: result.description,
                };

                return video;
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }

    public static async findVideoById(id: string): Promise<Video> {

        return Database.getSharedInstance()
            .select("*")
            .from<Video>("cl_videos")
            .where({
                id: id
            })
            .first()
            .then((video) => {

                return video;
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }

    public static async findVideosByCourseId(courseId: string): Promise<Video[]> {

        return Database.getSharedInstance()
            .select("*")
            .from<Video>("cl_videos")
            .where({
              courseId: courseId
            })
            .then((videos) => {

                return videos;
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }
}

export = VideoSelector;
