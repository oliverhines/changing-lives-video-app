import Database = require("../database");
import InternalServerError = require("../../errors/types/base/internal_server_error");
import User = require("../../../models/user");
const uuid = require("uuid");

class UserSelector {

    public static async findUserByEmail(email: string): Promise<User> {

        return Database.getSharedInstance()
            .select("*")
            .from<User>("cl_users")
            .where({
                email: email
            })
            .first()
            .then((user) => {

                return user;
            })
            .catch((error) => {

                throw new InternalServerError();
            })
    }

    public static async findUserByProxyId(proxyId: string): Promise<User> {

        return Database.getSharedInstance()
            .select("*")
            .from<User>("cl_users")
            .where({
                proxyId: proxyId
            })
            .first()
            .then((user) => {

                return user;
            })
            .catch((error) => {

                throw new InternalServerError();
            })
    }

    public static async createNewUser(email: string, hashedPassword: string, proxyId: string, privilegeLevel: string): Promise<User> {

        return Database.getSharedInstance()("cl_users")
            .insert({
                id: uuid(),
                email: email,
                hashedPassword: hashedPassword,
                proxyId: proxyId,
                privilegeLevel: privilegeLevel
            })
            .returning("*")
            .then(([result]) => {

                let user: User = {
                    id: result.id,
                    email: result.email,
                    hashedPassword: result.hashedPassword,
                    proxyId: result.proxyId,
                    privilegeLevel: result.privilegeLevel
                };

                return user;
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }
}

export = UserSelector;
