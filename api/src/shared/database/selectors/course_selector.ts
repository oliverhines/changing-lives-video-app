import Database = require("../database");
import InternalServerError = require("../../errors/types/base/internal_server_error");
import Course = require("../../../models/course");
const uuid = require("uuid");

class CourseSelector {

    public static async getCourses(): Promise<Course[]> {

        return Database.getSharedInstance()
            .select("*")
            .from<Course>("cl_courses")
            .then((result) => {

                return result;
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }

    public static async createNewCourse(name: string): Promise<Course> {

        return Database.getSharedInstance()("cl_courses")
            .insert({

                id: uuid(),
                name: name
            })
            .returning("*")
            .then(([result]) => {

                let course: Course = {
                    id: result.id,
                    name: result.name
                };

                return course;
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }

    public static async findCourseById(id: string): Promise<Course> {

        return Database.getSharedInstance()
            .select("*")
            .from<Course>("cl_courses")
            .where({
                id: id
            })
            .first()
            .then((course) => {

                return course;
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }
}

export = CourseSelector;
