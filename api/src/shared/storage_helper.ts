import {Bucket, File, GetSignedUrlConfig, GetSignedUrlResponse, Storage} from "@google-cloud/storage";
import Config from "../config";
import InternalServerError from "./errors/types/base/internal_server_error";

class StorageHelper {

    private static storage: Storage;
    private static bucket: Bucket;

    private static getStorage(): Storage {

        if (!this.storage) {

            this.storage = new Storage({projectId: Config.getSharedInstance().getGoogleCloudProjectId(),
                credentials: {client_email: Config.getSharedInstance().getGoogleCloudEmail(),
                        private_key: Config.getSharedInstance().getGoogleCloudPrivateKey()}});
        }

        return this.storage;
    }

    private static getBucket(): Bucket {

        if (!this.bucket) {

            this.bucket = this.getStorage().bucket(Config.getSharedInstance().getGoogleCloudBucketName());
        }

        return this.bucket;
    }

    public static async getSignedUploadUrl(filePathAndName: string, expiryDate: Date): Promise<string> {

        let file: File = this.getBucket().file(filePathAndName);

        // TODO Add contentMD5 and contentType headers
        let config: GetSignedUrlConfig = {
            action : "write",
            version: "v4",
            expires : expiryDate.getTime(),
        };

        let url: GetSignedUrlResponse = await file.getSignedUrl(config)
            .catch((error) => {

                throw new InternalServerError();
            });

        return url[0];
    }

    public static async getSignedDownloadUrl(filePathAndName: string, expiryDate: Date): Promise<string> {

        let file: File = this.getBucket().file(filePathAndName);

        // TODO Add contentMD5 and contentType headers
        let config: GetSignedUrlConfig = {
            action : "read",
            version: "v4",
            expires : expiryDate.getTime(),
        };

        let url: GetSignedUrlResponse = await file.getSignedUrl(config)
            .catch((error) => {

                throw new InternalServerError();
            });

        return url[0];
    }

    public static async fileExists(filePathAndName: string): Promise<boolean> {

        let file: File = this.getBucket().file(filePathAndName);

        return await file.exists()
            .then((result: [boolean]) => {

                return result[0];
            })
            .catch((error) => {

                throw new InternalServerError();
            });
    }
}

export = StorageHelper;
