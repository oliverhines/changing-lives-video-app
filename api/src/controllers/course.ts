import express = require("express");
import CourseCreationService = require("../services/course/course_creation_service");
import CourseListingService = require("../services/course/course_listing_service");
import Course = require("../models/course");
import CourseRetrievalService from "../services/course/course_retrieval_service";

class CourseController {

    public static async createCourse(req: express.Request, res: express.Response, next: express.NextFunction) {

        let name = req.body.name;

        CourseCreationService.createNewCourse(name)
            .then(() => {

                res.status(200)
                    .json();
            })
            .catch((error) => {

                next(error);
            });
    }

    public static async listCourses(req: express.Request, res: express.Response, next: express.NextFunction) {

        CourseListingService.listCourses()
            .then((courses: Course[]) => {

                res.status(200)
                    .json(courses);
            })
            .catch((error) => {

                next(error);
            });
    }

    public static async getCourse(req: express.Request, res: express.Response, next: express.NextFunction) {

        let courseId: string = req.query.courseId;

        CourseRetrievalService.getCourseById(courseId)
            .then((course) => {

                res.status(200)
                    .json(course);
            })
            .catch((error) => {

                next(error);
            });
    }
}

export = CourseController;
