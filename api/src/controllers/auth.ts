import express = require("express");
import RefreshTokenService = require("../services/auth/refresh_token_service");
import UserRegistrationService = require("../services/auth/user_registration_service");
import TokenResponse = require("../services/auth/tokens/token_response");
import UserLoginService = require("../services/auth/user_login_service");

class AuthController {

    private static HEADERS_PAYLOAD_COOKIE = "changing-lives-headers-payload";
    private static SIGNATURE_COOKIE = "changing-lives-signature";

    public static async registerNewUser(req: express.Request, res: express.Response, next: express.NextFunction) {

        let email: string = req.body.email;
        let password: string = req.body.password;

        UserRegistrationService.registerNewUser(email, password)
            .then((tokenResponse: TokenResponse) => {

                // TODO Make this secure in production
                res.cookie(AuthController.HEADERS_PAYLOAD_COOKIE, tokenResponse.headersAndPayload, { expires: tokenResponse.expiresAt });
                res.cookie(AuthController.SIGNATURE_COOKIE, tokenResponse.signature, { httpOnly: true });

                res.status(200)
                    .json();
            })
            .catch((error) => {

                next(error);
            });
    }

    public static async login(req: express.Request, res: express.Response, next: express.NextFunction) {

        let email: string = req.body.email;
        let givenPassword: string = req.body.password;

        UserLoginService.login(email, givenPassword)
            .then((tokenResponse: TokenResponse) => {

                // TODO Make this secure in production
                res.cookie(AuthController.HEADERS_PAYLOAD_COOKIE, tokenResponse.headersAndPayload, { expires: tokenResponse.expiresAt });
                res.cookie(AuthController.SIGNATURE_COOKIE, tokenResponse.signature, { httpOnly: true });

                res.status(200)
                    .json();
            })
            .catch((error) => {

                next(error);
            });
    }

    public static async requireAuthentication(req: express.Request, res: express.Response, next: express.NextFunction) {

        let headersAndPayload = req.cookies[AuthController.HEADERS_PAYLOAD_COOKIE];
        let signature = req.cookies[AuthController.SIGNATURE_COOKIE];

        let token = [headersAndPayload, signature]
            .join(".");

        RefreshTokenService.refreshAuthToken(token)
            .then((tokenResponse) => {

                // TODO Make this secure in production
                res.cookie(AuthController.HEADERS_PAYLOAD_COOKIE, tokenResponse.headersAndPayload, { expires: tokenResponse.expiresAt });
                res.cookie(AuthController.SIGNATURE_COOKIE, tokenResponse.signature, { httpOnly: true });

                res.locals.userId = tokenResponse.userId;

                next();
            })
            .catch((error) => {

                next(error);
            });
    }
}

export = AuthController;
