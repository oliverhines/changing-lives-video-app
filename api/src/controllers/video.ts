import express = require("express");
import VideoCreationService from "../services/video/video_creation_service";
import VideoDownloadService from "../services/video/video_download_service";
import VideoRetrievalService from "../services/video/video_retrieval_service";
import Video from "../models/video";
import VideoListingService from "../services/video/video_listing_service";

class VideoController {

    public static async createVideo(req: express.Request, res: express.Response, next: express.NextFunction) {

        let courseId: string = req.body.courseId;
        let title: string = req.body.title;
        let description: string = req.body.description;

        VideoCreationService.createNewVideo(courseId, title, description)
            .then((uploadURL: string) => {

                res.status(200)
                    .json({uploadURL: uploadURL});
            })
            .catch((error) => {

                next(error);
            });
    }

    public static getDownloadURL(req: express.Request, res: express.Response, next: express.NextFunction) {

        let videoId: string = req.query.videoId;

        VideoDownloadService.getDownloadURL(videoId)
            .then((downloadURL: string) => {

                res.status(200)
                    .json({downloadURL: downloadURL});
            })
            .catch((error) => {

                next(error);
            });
    }

    public static getVideo(req: express.Request, res: express.Response, next: express.NextFunction) {

        let videoId: string = req.query.videoId;

        VideoRetrievalService.getVideoById(videoId)
            .then((video: Video) => {

                res.status(200)
                    .json(video);
            })
            .catch((error) => {

                next(error);
            });
    }

    public static listVideosForCourse(req: express.Request, res: express.Response, next: express.NextFunction) {

        let courseId: string = req.query.courseId;

        VideoListingService.listVideosForCourse(courseId)
            .then((videos: Video[]) => {
                res.status(200)
                    .json(videos);
            })
            .catch((error) => {

                next(error);
            });
    }
}

export = VideoController;
