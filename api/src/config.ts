class Config {

    public static MISSING_ENVIRONMENT_VARIABLE_ERROR: Error = new Error("There is a missing environment variable");

    private static sharedInstance: Config;

    public checkAllVariablesSet() {

        if (Config.isEmpty(this.getJWTSecret()) || Config.isEmpty(this.getGoogleCloudProjectId()) ||
            Config.isEmpty(this.getGoogleCloudEmail()) || Config.isEmpty(this.getGoogleCloudPrivateKey()) ||
            Config.isEmpty(this.getDatabaseConnectionString()) || Config.isEmpty(this.getGoogleCloudBucketName())) {

            throw Config.MISSING_ENVIRONMENT_VARIABLE_ERROR;
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }

    public static getSharedInstance(): Config {

        if (!this.sharedInstance) {
            this.sharedInstance = new Config();
        }

        return this.sharedInstance;
    }

    public getJWTSecret(): string {

        if (Config.isEmpty(process.env.JWT_SECRET)) {

            throw Config.MISSING_ENVIRONMENT_VARIABLE_ERROR;
        }

        return process.env.JWT_SECRET;
    }

    public getGoogleCloudProjectId(): string {

        if (Config.isEmpty(process.env.GCP_PROJECT_ID)) {

            throw Config.MISSING_ENVIRONMENT_VARIABLE_ERROR;
        }

        return process.env.GCP_PROJECT_ID;
    }

    public getGoogleCloudEmail(): string {

        if (Config.isEmpty(process.env.GCP_EMAIL)) {

            throw Config.MISSING_ENVIRONMENT_VARIABLE_ERROR;
        }

        return process.env.GCP_EMAIL;
    }

    public getGoogleCloudPrivateKey(): string {

        if (Config.isEmpty(process.env.GCP_PRIVATE_KEY)) {

            throw Config.MISSING_ENVIRONMENT_VARIABLE_ERROR;
        }

        // Replace any double escaped string characters (this is a docker issue with character escaping in .env files)
        return process.env.GCP_PRIVATE_KEY.replace(/\\n/g, "\n");
    }

    public getDatabaseConnectionString(): string {

        if (Config.isEmpty(process.env.DB_CONNECTION_STRING)) {

            throw Config.MISSING_ENVIRONMENT_VARIABLE_ERROR
        }

        return process.env.DB_CONNECTION_STRING;
    }

    public getGoogleCloudBucketName(): string {

        if (Config.isEmpty(process.env.GCP_BUCKET_NAME)) {

            throw Config.MISSING_ENVIRONMENT_VARIABLE_ERROR
        }

        return process.env.GCP_BUCKET_NAME;
    }
}

export = Config;
