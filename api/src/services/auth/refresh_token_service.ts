import JWTService = require("./tokens/jwt_service");
import TokenClaims = require("./tokens/token_claims");
import TokenGenerator = require("./tokens/token_generator");
import TokenResponse = require("./tokens/token_response");
import UnauthorisedError = require("../../shared/errors/types/base/unauthorised_error");
import UserSelector = require("../../shared/database/selectors/user_selector");
import InvalidTokenError = require("../../shared/errors/types/specific/invalid_token_error");

class RefreshTokenService {

    public static AUTHORISATION_MISSING_ERROR = new UnauthorisedError("authorisation_missing", "Authorisation required for this endpoint. Please supply authorisation token.");

    public static async refreshAuthToken(token: string): Promise<TokenResponse> {

        this.checkForNullFields(token);

        let claims: TokenClaims = JWTService.verifyToken(token);

        let givenUserId: string = claims.userId;
        let givenProxyId: string = claims.proxyId;

        let user = await UserSelector.findUserByProxyId(givenProxyId)
            .catch((error) => {

                throw error;
            });

        if (user == null) {

            throw new InvalidTokenError();
        }

        if (user.id !== givenUserId) {

            throw new InvalidTokenError();
        }

        return TokenGenerator.generateAuthToken(givenUserId, givenProxyId);
    }

    private static checkForNullFields(token): void {
        if (this.isEmpty(token)) {

            throw this.AUTHORISATION_MISSING_ERROR;
        }

        if (token.split(".").length !== 3) {

            throw this.AUTHORISATION_MISSING_ERROR;
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = RefreshTokenService;
