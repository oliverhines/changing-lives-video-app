enum PrivilegeLevels {

    User = "user",
    Admin = "admin",
    SuperAdmin = "super-admin"
}

export = PrivilegeLevels;
