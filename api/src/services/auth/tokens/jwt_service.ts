import crypto = require("crypto");
import TokenClaims = require("./token_claims");
import TokenResponse = require("./token_response");
import InvalidTokenError = require("../../../shared/errors/types/specific/invalid_token_error");
import Config = require("../../../config");

class JWTService {

    private static ALGORITHM_HEADER = "HS256";
    private static SIGNING_METHOD = "sha256";
    private static ISSUER_CLAIM = "changing-lives-server";
    private static AUDIENCE_CLAIM = "changing-lives-client";

    public static verifyToken(token: string): TokenClaims {

        let segments = token.split(".");

        if (segments.length !== 3) {

            throw new InvalidTokenError();
        }

        let headerSegment = segments[0];
        let payloadSegment = segments[1];
        let signatureSegment = segments[2];

        let signingInput = [headerSegment, payloadSegment].join(".");

        if (!this.verify(signingInput, signatureSegment)) {

            throw new InvalidTokenError();
        }

        let payload = JSON.parse(this.base64UrlDecode(payloadSegment));

        let expiredAtMilliseconds: number = payload.exp * 1000;

        if (expiredAtMilliseconds && Date.now() > expiredAtMilliseconds) {

            throw new InvalidTokenError();
        }

        return new TokenClaims(payload.sub, payload.jti, payload.iss, payload.aud, payload.exp, payload.iat);
    }

    public static generateToken(userId: string, proxyId: string, issuedAt: Date, expiresAt: Date): TokenResponse {

        let header = {
            alg: this.ALGORITHM_HEADER,
        };

        let issuedAtSeconds: number = Math.floor(issuedAt.getTime() / 1000);
        let expiresAtSeconds: number = Math.floor(expiresAt.getTime() / 1000);

        let payload = {
            sub: userId,
            jti: proxyId,
            iss: this.ISSUER_CLAIM,
            aud: this.AUDIENCE_CLAIM,
            exp: expiresAtSeconds,
            iat: issuedAtSeconds,
        };

        let encodedHeader = this.base64UrlEncode(JSON.stringify(header));
        let encodedPayload = this.base64UrlEncode(JSON.stringify(payload));
        let encodedSignature = this.sign(encodedHeader + "." + encodedPayload);

        let headerAndPayload = [encodedHeader, encodedPayload]
            .join(".");

        return new TokenResponse(headerAndPayload, encodedSignature, expiresAt, userId);
    }

    private static sign(input: string): string {

        let signedInput = crypto.createHmac(this.SIGNING_METHOD, Config.getSharedInstance().getJWTSecret())
            .update(input)
            .digest("base64");

        return this.base64UrlEscape(signedInput);
    }

    private static verify(input: string, signature: string): boolean {

        return signature === this.sign(input);
    }

    private static base64UrlEncode(input: string): string {

        return this.base64UrlEscape(Buffer.from(input).toString("base64"));
    }

    private static base64UrlDecode(input: string): string {

        return Buffer.from(this.base64UrlUnescape(input), "base64").toString();
    }

    private static base64UrlEscape(input: string): string {
        return input.replace(/\+/g, "-")
            .replace(/\//g, "_")
            .replace(/=/g, "");
    }

    private static base64UrlUnescape(input: string): string {

        input += new Array(5 - input.length % 4).join("=");
        return input.replace(/\-/g, "+").replace(/_/g, "/");
    }
}

export = JWTService;
