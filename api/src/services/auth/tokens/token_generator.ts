import JWTService = require("./jwt_service");
import TokenResponse = require("./token_response");

class TokenGenerator {

    private static ONE_MINUTE = 60 * 1000;
    private static AUTH_TOKEN_LIFE = TokenGenerator.ONE_MINUTE * 15;

    public static generateAuthToken(userId: string, proxyId: string): TokenResponse {

        let issuedAt = new Date(Date.now());
        let expiresAt = new Date(issuedAt.getTime() + this.AUTH_TOKEN_LIFE);

        return JWTService.generateToken(userId, proxyId, issuedAt, expiresAt);
    }
}

export = TokenGenerator;
