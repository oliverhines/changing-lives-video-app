import chai = require("chai");
import sinon from "sinon";
import JWTService = require("./jwt_service");
import TokenClaims = require("./token_claims");
import TokenResponse = require("./token_response");
import Config = require("../../../config");
import InvalidTokenError = require("../../../shared/errors/types/specific/invalid_token_error");

let expect = chai.expect;
let assert = chai.assert;

describe("JWTService class", () => {

    const TEST_USER_ID = "test-user-id";
    const TEST_PROXY_ID = "test-proxy-id";
    const TEST_ISSUED_AT = new Date(2000, 1, 1, 0, 0, 0, 0);
    const TEST_EXPIRES_AT = new Date(2000, 1, 1, 1, 0, 0, 0);

    const TEST_JWT_SECRET = "aSecretString";

    const EXPECTED_TOKEN = {

        HEADERS_PAYLOAD : "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0LXVzZXItaWQiLCJqdGkiOiJ0ZXN0LXByb3h5LWlkIiwiaXNzIjoiY2hhbmdpbmctbGl2ZXMtc2VydmVyIiwiYXVkIjoiY2hhbmdpbmctbGl2ZXMtY2xpZW50IiwiZXhwIjo5NDkzNjY4MDAsImlhdCI6OTQ5MzYzMjAwfQ",
        SIGNATURE : "cGyWVcH-ySh7TamUtZjDC_4j9S5HYN-DtqJobNY2Oac",
    };

    const TOO_FEW_PARTS_TOKEN = "abc.def";
    // This token was signed with "anInvalidString"
    const INVALID_SIGNATURE_TOKEN = "eyJ0eXAiOiJ0ZXN0LXR5cGUiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0LXVzZXItaWQiLCJqdGkiOiJ0ZXN0LXByb3h5LWlkIiwiaXNzIjoiYWRkdXJlbnQtc2VydmVyIiwiYXVkIjoiYWRkdXJlbnQtY2xpZW50IiwiZXhwIjo5NDkzNjY4MDAsImlhdCI6OTQ5MzYzMjAwfQ.hobQHqjORHiJLy2XwMUm8bCDqGe9ogE1VKKzG4pul5U";
    // This token expired on TEST_EXPIRES_AT
    const EXPIRED_TOKEN = "eyJ0eXAiOiJ0ZXN0LXR5cGUiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0LXVzZXItaWQiLCJqdGkiOiJ0ZXN0LXByb3h5LWlkIiwiaXNzIjoiYWRkdXJlbnQtc2VydmVyIiwiYXVkIjoiYWRkdXJlbnQtY2xpZW50IiwiZXhwIjo5NDkzNjY4MDAsImlhdCI6OTQ5MzYzMjAwfQ.4yGqKGWYegvS0DZn3C4zHAAEh35ejSknOZt8dG3RVJc";
    // This token was signed with "aSecretString" and expires on TEST_EXPIRES_AT, issued at TEST_ISSUED_AT, typ = TEST_TOKEN_TYPE
    const VALID_TOKEN = "eyJ0eXAiOiJ0ZXN0LXR5cGUiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0LXVzZXItaWQiLCJqdGkiOiJ0ZXN0LXByb3h5LWlkIiwiaXNzIjoiYWRkdXJlbnQtc2VydmVyIiwiYXVkIjoiYWRkdXJlbnQtY2xpZW50IiwiZXhwIjo5NDkzNjY4MDAsImlhdCI6OTQ5MzYzMjAwfQ.4yGqKGWYegvS0DZn3C4zHAAEh35ejSknOZt8dG3RVJc";

    let configMock: sinon.SinonMock;
    let dateMock: sinon.SinonMock;

    before(async () => {

        configMock = sinon.mock(Config.prototype);
        dateMock = sinon.mock(Date);
    });

    describe("verifyToken function", () => {

        it("Throws error for token that doesn't have 3 segments", async () => {

            expect(() => JWTService.verifyToken(TOO_FEW_PARTS_TOKEN))
                .to.throw(new InvalidTokenError().reason);
        });

        it("Throws error for token that has an invalid signature", async () => {

            configMock.expects("getJWTSecret")
                .returns(TEST_JWT_SECRET);

            expect(() => JWTService.verifyToken(INVALID_SIGNATURE_TOKEN))
                .to.throw(new InvalidTokenError().reason);
        });

        it("Throws error for token that is valid but expired", async () => {

            configMock.expects("getJWTSecret")
                .returns(TEST_JWT_SECRET);

            expect(() => JWTService.verifyToken(EXPIRED_TOKEN))
                .to.throw(new InvalidTokenError().reason);
        });

        it("Returns correct claims for token that is valid and unexpired", async () => {

            configMock.expects("getJWTSecret")
                .returns(TEST_JWT_SECRET);

            dateMock.expects("now")
                .returns(TEST_ISSUED_AT.getTime());

            let actualClaims: TokenClaims;

            try {

                actualClaims = JWTService.verifyToken(VALID_TOKEN);

                assert.equal(actualClaims.issuedAt, TEST_ISSUED_AT.getTime() / 1000);
                assert.equal(actualClaims.expiresAt, TEST_EXPIRES_AT.getTime() / 1000);
                assert.equal(actualClaims.userId, TEST_USER_ID);
                assert.equal(actualClaims.proxyId, TEST_PROXY_ID);

            } catch (error) {

                assert.equal(true, false, `Failed with exception: ${error}`);
            }
        });
    });

    describe("generateToken function", () => {

        it("Correctly generates token", async () => {

            configMock.expects("getJWTSecret")
                .returns(TEST_JWT_SECRET);

            let tokenResponse: TokenResponse = JWTService.generateToken(TEST_USER_ID, TEST_PROXY_ID, TEST_ISSUED_AT, TEST_EXPIRES_AT);

            expect(tokenResponse.headersAndPayload)
                .to.be.equal(EXPECTED_TOKEN.HEADERS_PAYLOAD);

            expect(tokenResponse.signature)
                .to.be.equal(EXPECTED_TOKEN.SIGNATURE);

            expect(tokenResponse.expiresAt)
                .to.be.equal(TEST_EXPIRES_AT);

            expect(tokenResponse.userId)
                .to.be.equal(TEST_USER_ID);
        });
    });

    after(() => {

        configMock.restore();
        dateMock.restore();
    });
});
