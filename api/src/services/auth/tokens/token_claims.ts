class TokenClaims {

    public userId: string;
    public proxyId: string;
    public issuer: string;
    public audience: string;
    public expiresAt: number;
    public issuedAt: number;

    constructor(userId: string, proxyId: string, issuer: string, audience: string, expiresAt: number, issuedAt: number) {

        this.userId = userId;
        this.proxyId = proxyId;
        this.issuer = issuer;
        this.audience = audience;
        this.expiresAt = expiresAt;
        this.issuedAt = issuedAt;
    }
}

export = TokenClaims;
