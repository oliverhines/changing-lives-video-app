class TokenResponse {

    public headersAndPayload: string;
    public signature: string;
    public expiresAt: Date;
    public userId: string;

    constructor(headerAndPayload: string, signature: string, expiresAt: Date, userId: string) {

        this.headersAndPayload = headerAndPayload;
        this.signature = signature;
        this.expiresAt = expiresAt;
        this.userId = userId;
    }
}

export = TokenResponse;
