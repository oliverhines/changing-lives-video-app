import chai = require("chai");
import sinon = require("sinon");
import JWTService = require("./jwt_service");
import TokenService = require("./token_generator");
import TokenResponse = require("./token_response");

let expect = chai.expect;

describe("TokenGenerator class", () => {

    const TEST_USER_ID = "test-id";
    const TEST_PROXY_ID = "test-proxy-id";

    const TEST_ISSUED_AT = new Date(2000, 1, 1, 0, 0, 0, 0);
    const TEST_AUTH_EXPIRES_AT = new Date(2000, 1, 1, 0, 15, 0, 0);

    let JWTServiceMock: sinon.SinonMock;
    let dateMock: sinon.SinonMock;

    before(async () => {

        JWTServiceMock = sinon.mock(JWTService);
        dateMock = sinon.mock(Date);
    });

    describe("generateAuthTokenOnly function", () => {

        it("Calls JWTService with correct parameters and returns correct response", async () => {

            let expectedTokenResponse: TokenResponse = new TokenResponse("example headers and payload",
                "example signature", TEST_AUTH_EXPIRES_AT, TEST_USER_ID);

            JWTServiceMock.expects("generateToken")
                .withExactArgs(TEST_USER_ID, TEST_PROXY_ID, TEST_ISSUED_AT, TEST_AUTH_EXPIRES_AT)
                .returns(expectedTokenResponse);

            dateMock.expects("now")
                .returns(TEST_ISSUED_AT.getTime());

            let response: TokenResponse = TokenService.generateAuthToken(TEST_USER_ID, TEST_PROXY_ID);

            expect(response)
                .to.be.equal(expectedTokenResponse);
        });
    });

    after(() => {

        JWTServiceMock.restore();
        dateMock.restore();
    });
});
