import TokenResponse = require("./tokens/token_response");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import ValidationError = require("../../shared/errors/types/specific/validation_error");
import UserSelector = require("../../shared/database/selectors/user_selector");
import PasswordHashingService = require("./password_hashing_service");
import ProxyIdGenerator = require("./proxy_id_generator");
import PrivilegeLevels = require("./privilege_levels");
import User = require("../../models/user");
import TokenGenerator = require("./tokens/token_generator");

class UserRegistrationService {

    public static EMAIL_LENGTH_ERROR = new ValidationError("email", "length", "Email length must be no more than 254 characters");
    public static EMAIL_FORMAT_ERROR = new ValidationError("email", "format", "Email must be in a valid format");
    public static EMAIL_UNIQUE_ERROR = new ValidationError("email", "unique", "Email must be unique");
    public static PASSWORD_LENGTH_ERROR = new ValidationError("password", "length", "Password length must be between 8 and 50 characters");

    public static async registerNewUser(email: string, password: string): Promise<TokenResponse> {

        this.checkForNullFields(email, password);

        if (!this.emailLengthValid(email)) {

            throw this.EMAIL_LENGTH_ERROR;
        }

        if (!this.emailFormatValid(email)) {

            throw this.EMAIL_FORMAT_ERROR;
        }

        email = this.getComparableEmail(email);

        let emailIsUnique: boolean = await this.emailIsUnique(email);

        if (!emailIsUnique) {

            throw this.EMAIL_UNIQUE_ERROR;
        }

        if (!this.passwordLengthValid(password)) {

            throw this.PASSWORD_LENGTH_ERROR;
        }

        let hashedPassword: string = await PasswordHashingService.hashPassword(password)
            .catch((error) => {

                throw error;
            });

        let proxyId: string = await ProxyIdGenerator.generateNewProxyId()
            .catch((error) => {

                throw error;
            });

        let privilegeLevel: string = PrivilegeLevels.User;

        let newUser: User = await UserSelector.createNewUser(email, hashedPassword, proxyId, privilegeLevel)
            .catch((error) => {

                throw error;
            });

        return TokenGenerator.generateAuthToken(newUser.id, newUser.proxyId);
    }

    private static emailLengthValid(email: string): boolean {

        return email.length < 255;
    }

    private static emailFormatValid(email: string): boolean {

        let emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return emailPattern.test(email);
    }

    private static getComparableEmail(email: string): string {

        let emailParts = email.split("@");

        let domain = emailParts.pop();

        emailParts.push(domain.toLowerCase());

        return emailParts.join("@");
    }

    private static async emailIsUnique(email: string): Promise<boolean> {

        let existingUser = await UserSelector.findUserByEmail(email)
            .catch((error) => {

                throw error;
            });

        return existingUser == null;
    }

    private static passwordLengthValid(password: string): boolean {

        return password.length > 7 && password.length < 51;
    }

    private static checkForNullFields(email: string, password: string): void {
        if (this.isEmpty(email) || this.isEmpty(password)) {
            throw new MissingFieldsError();
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = UserRegistrationService;
