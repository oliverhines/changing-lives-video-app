import bcrypt = require("bcrypt");
import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import sinon = require("sinon");
import InternalServerError = require("../../shared/errors/types/base/internal_server_error");
import PasswordHashingService = require("./password_hashing_service");

chai.use(chaiAsPromised);

chai.should();

let expect = chai.expect;

describe("PasswordHashingService class", () => {

    const TEST_PASSWORD = "someTestPassword";
    const SALTING_ROUNDS = 10;

    const TEST_HASHED_PASSWORD = "someJumbledString";

    let bcryptMock: sinon.SinonMock;

    before(async () => {

        bcryptMock = sinon.mock(bcrypt);
    });

    describe("hashPassword function", () => {

        it("Check function calls bcrypt library with correct args", async () => {

            bcryptMock.expects("hash")
                .withExactArgs(TEST_PASSWORD, SALTING_ROUNDS)
                .resolves(TEST_HASHED_PASSWORD);

            await expect(PasswordHashingService.hashPassword(TEST_PASSWORD))
                .to.eventually.be.equal(TEST_HASHED_PASSWORD);
        });

        it("Throws correct exception for some library error", async () => {

            bcryptMock.expects("hash")
                .withExactArgs(TEST_PASSWORD, SALTING_ROUNDS)
                .rejects(new Error("This is some random error thrown by bcrypt"));

            await expect(PasswordHashingService.hashPassword(TEST_PASSWORD))
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });
    });

    describe("verifyPassword function", () => {

        it("Check function calls bcrypt library with correct args", async () => {

            bcryptMock.expects("compare")
                .withExactArgs(TEST_PASSWORD, TEST_HASHED_PASSWORD)
                .resolves(true);

            await expect(PasswordHashingService.verifyPassword(TEST_HASHED_PASSWORD, TEST_PASSWORD))
                .to.eventually.be.equal(true);
        });

        it("Throws correct exception for some library error", async () => {

            bcryptMock.expects("compare")
                .withExactArgs(TEST_PASSWORD, TEST_HASHED_PASSWORD)
                .rejects(new Error("Another error thrown by bcrypt"));

            await expect(PasswordHashingService.verifyPassword(TEST_HASHED_PASSWORD, TEST_PASSWORD))
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });
    });

    after(() => {

        bcryptMock.restore();
    });
});
