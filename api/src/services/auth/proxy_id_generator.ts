import crypto = require("crypto");
import UserSelector = require("../../shared/database/selectors/user_selector");

class ProxyIdGenerator {

    public static async generateNewProxyId(): Promise<string> {

        let uniqueIdFound: boolean = false;
        let proposedId: string;
        while (!uniqueIdFound) {

            proposedId = crypto.randomBytes(8).toString("hex");

            let existingUser = await UserSelector.findUserByProxyId(proposedId)
                .catch((error) => {

                    throw error;
                });

            if (existingUser == null) {

                uniqueIdFound = true;
            }
        }

        return proposedId;
    }
}

export = ProxyIdGenerator;
