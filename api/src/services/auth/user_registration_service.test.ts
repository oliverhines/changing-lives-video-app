import chai = require("chai");
import sinon = require("sinon");
import UserRegistrationService = require("./user_registration_service");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import UserSelector = require("../../shared/database/selectors/user_selector");
import User = require("../../models/user");
import TokenGenerator = require("./tokens/token_generator");
import InternalServerError = require("../../shared/errors/types/base/internal_server_error");
import ProxyIdGenerator = require("./proxy_id_generator");
import PasswordHashingService = require("./password_hashing_service");
import TokenResponse = require("./tokens/token_response");

let expect = chai.expect;

describe("UserRegistrationService class", () => {

    const EXISTING_EMAIL: string = "existing@example.com";

    const EXISTING_USER: User = {
        id: "some_id",
        email: EXISTING_EMAIL,
        hashedPassword: "wedjdadasdd",
        proxyId: "someTestProxyId",
        privilegeLevel: "user"
    };

    let userSelectorMock: sinon.SinonMock;
    let tokenGeneratorMock: sinon.SinonMock;
    let proxyIdGeneratorMock: sinon.SinonMock;
    let passwordHashingServiceMock: sinon.SinonMock;

    const VALID_TEST_EMAIL: string = "test@example.com";
    const VALID_TEST_PASSWORD: string = "examplePassword";

    const TEST_HASHED_PASSWORD: string = "someJumbledUpStuff";
    const TEST_PROXY_ID: string = "exampleId";

    before(async () => {

        userSelectorMock = sinon.mock(UserSelector);
        tokenGeneratorMock = sinon.mock(TokenGenerator);
        proxyIdGeneratorMock = sinon.mock(ProxyIdGenerator);
        passwordHashingServiceMock = sinon.mock(PasswordHashingService);
    });

    describe("registerNewUser function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(UserRegistrationService.registerNewUser(VALID_TEST_EMAIL, null))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(UserRegistrationService.registerNewUser("", VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(UserRegistrationService.registerNewUser(VALID_TEST_EMAIL, "      "))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when email is too long", async () => {

            await expect(UserRegistrationService.registerNewUser("SwwKbNwesH0QC2xvXZ2wNgJ93bwhv3zlVF6XddWPK1EzMlZ3AMfRtAQMyGVmYB3NmxZ8PFMNjQKoDsfRVog3vypwmCjMYp0kg7nCQuFP59Jiuphx9YeYiRhiPfUVC5988RzjffYvi7gLDpFQA0aQow6x3P7D5QEsAQbyGrciVWC10JWKuY3lwqezmPncunKDIPLEmK8OhDjymSvG2QhLELhQiDafuI988ErBpgaZBaHcBAe086jMK1qyHK4lgg@example.com", VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(UserRegistrationService.EMAIL_LENGTH_ERROR.reason);
        });

        it("Exception correctly thrown when email uses invalid format", async () => {

            await expect(UserRegistrationService.registerNewUser("@ex.ac.uk", VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(UserRegistrationService.EMAIL_FORMAT_ERROR.reason);
        });

        it("Exception correctly thrown when email is already in use", async () => {

            userSelectorMock.expects("findUserByEmail")
                .withExactArgs(EXISTING_EMAIL)
                .resolves(EXISTING_USER);

            await expect(UserRegistrationService.registerNewUser(EXISTING_EMAIL, VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(UserRegistrationService.EMAIL_UNIQUE_ERROR.reason);
        });

        it("Exception correctly thrown when email uses different case for domain name", async () => {

            userSelectorMock.expects("findUserByEmail")
                .withExactArgs("funny@case.com")
                .resolves(EXISTING_USER);

            await expect(UserRegistrationService.registerNewUser("funny@CAse.cOm", VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(UserRegistrationService.EMAIL_UNIQUE_ERROR.reason);
        });

        it("Exception correctly thrown when password is too short", async () => {

            userSelectorMock.expects("findUserByEmail")
                .resolves(null);

            await expect(UserRegistrationService.registerNewUser(VALID_TEST_EMAIL, "short"))
                .to.eventually.be.rejectedWith(UserRegistrationService.PASSWORD_LENGTH_ERROR.reason);
        });

        it("Exception correctly thrown when password is too long", async () => {

            userSelectorMock.expects("findUserByEmail")
                .resolves(null);

            await expect(UserRegistrationService.registerNewUser(VALID_TEST_EMAIL, "SwwKbNwesddH0QC2xvXZ2wNgJ93bwhv3zlVF6XddWPK1EzMlZ3AMfRtAQMyGVmYB3NmxZ8PFMNjQKoDsfRVog3vypwmCjMYp0kg7nCQuFP59Jiuphx9YeYiRhiPfUVC5988RzjffYvi7gLDpFQA0aQow6x3P7D5QEsAQbyGrciVWC10JWKuY3lwqezmPncunKDIPLEmK8OhDjymSvG2QhLELhQiDafuI988ErBpgaZBaHcBAe086jMK1qyHK4lgg"))
                .to.eventually.be.rejectedWith(UserRegistrationService.PASSWORD_LENGTH_ERROR.reason);
        });

        it("Exception correctly thrown when there is a problem creating the user object", async () => {

            userSelectorMock.expects("findUserByEmail")
                .resolves(null);

            proxyIdGeneratorMock.expects("generateNewProxyId")
                .resolves(TEST_PROXY_ID);

            passwordHashingServiceMock.expects("hashPassword")
                .withExactArgs(VALID_TEST_PASSWORD)
                .resolves(TEST_HASHED_PASSWORD);

            userSelectorMock.expects("createNewUser")
                .rejects(new InternalServerError());

            await expect(UserRegistrationService.registerNewUser(VALID_TEST_EMAIL, VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });

        it("Correct user registered when all valid parameters are passed and tokens returned", async () => {

            userSelectorMock.expects("findUserByEmail")
                .resolves(null);

            proxyIdGeneratorMock.expects("generateNewProxyId")
                .resolves(TEST_PROXY_ID);

            passwordHashingServiceMock.expects("hashPassword")
                .withExactArgs(VALID_TEST_PASSWORD)
                .resolves(TEST_HASHED_PASSWORD);

            let expectedUser: User = {
                id: "some id",
                email: VALID_TEST_EMAIL,
                hashedPassword: TEST_HASHED_PASSWORD,
                proxyId: TEST_PROXY_ID,
                privilegeLevel: "user"
            };

            userSelectorMock.expects("createNewUser")
                .resolves(expectedUser);

            let expectedTokenResponse = new TokenResponse("the auth token", "the refresh token", null, expectedUser.id);

            tokenGeneratorMock.expects("generateAuthToken")
                .withExactArgs(expectedUser.id, expectedUser.proxyId)
                .returns(expectedTokenResponse);

            await expect(UserRegistrationService.registerNewUser(VALID_TEST_EMAIL, VALID_TEST_PASSWORD))
                .to.eventually.be.equal(expectedTokenResponse);
        });
    });

    after(() => {

        userSelectorMock.restore();
        tokenGeneratorMock.restore();
        proxyIdGeneratorMock.restore();
        passwordHashingServiceMock.restore();
    });
});
