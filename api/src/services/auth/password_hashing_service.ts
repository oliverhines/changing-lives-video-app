import bcrypt = require("bcrypt");
import InternalServerError = require("../../shared/errors/types/base/internal_server_error");

class PasswordHashingService {

    public static hashPassword(password: string): Promise<string> {

        return bcrypt.hash(password, 10)
            .then((hash) => {

                return hash;
            })
            .catch(() => {

                throw new InternalServerError();
            });
    }

    public static verifyPassword(saltedHash: string, givenPassword: string): Promise<boolean> {

        return bcrypt.compare(givenPassword, saltedHash)
            .then((result) => {

                return result;
            })
            .catch(() => {

                throw new InternalServerError();
            });
    }
}

export = PasswordHashingService;
