import chai = require("chai");
import sinon = require("sinon");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import UserSelector = require("../../shared/database/selectors/user_selector");
import User = require("../../models/user");
import TokenGenerator = require("./tokens/token_generator");
import PasswordHashingService = require("./password_hashing_service");
import TokenResponse = require("./tokens/token_response");
import UserLoginService = require("./user_login_service");

let expect = chai.expect;

describe("UserLoginService class", () => {

    const VALID_TEST_EMAIL: string = "test@example.com";
    const VALID_TEST_PASSWORD: string = "examplePassword";
    const VALID_HASHED_PASSWORD: string = "someJumbledHash";

    const EXISTING_USER: User = {
        id: "some-id",
        email: VALID_TEST_EMAIL,
        hashedPassword: VALID_HASHED_PASSWORD,
        proxyId: "some proxy id",
        privilegeLevel: "user"
    };

    const INVALID_TEST_EMAIL: string = "invalid@example.com";
    const INVALID_TEST_PASSWORD: string = "anInvalidPassword";

    let userSelectorMock: sinon.SinonMock;
    let tokenGeneratorMock: sinon.SinonMock;
    let passwordHashingServiceMock: sinon.SinonMock;

    before(async () => {

        userSelectorMock = sinon.mock(UserSelector);
        tokenGeneratorMock = sinon.mock(TokenGenerator);
        passwordHashingServiceMock = sinon.mock(PasswordHashingService);
    });

    describe("login function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(UserLoginService.login(VALID_TEST_EMAIL, null))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(UserLoginService.login("", VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(UserLoginService.login("       ", VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when the user does not exist", async () => {

            userSelectorMock.expects("findUserByEmail")
                .withExactArgs(INVALID_TEST_EMAIL)
                .resolves(null);

            await expect(UserLoginService.login(INVALID_TEST_EMAIL, VALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(UserLoginService.INCORRECT_EMAIL_PASSWORD_ERROR.reason);
        });

        it("Exception correctly thrown when the provided password does not hash to the expected value", async () => {

            let existingUser: User = {

                id: "some-id",
                email: VALID_TEST_EMAIL,
                hashedPassword: VALID_HASHED_PASSWORD,
                proxyId: "some proxy id",
                privilegeLevel: "user"
            };

            userSelectorMock.expects("findUserByEmail")
                .withExactArgs(VALID_TEST_EMAIL)
                .resolves(existingUser);

            passwordHashingServiceMock.expects("verifyPassword")
                .withExactArgs(VALID_HASHED_PASSWORD, INVALID_TEST_PASSWORD)
                .resolves(false);

            await expect(UserLoginService.login(VALID_TEST_EMAIL, INVALID_TEST_PASSWORD))
                .to.eventually.be.rejectedWith(UserLoginService.INCORRECT_EMAIL_PASSWORD_ERROR.reason);
        });

        it("User logged in and returned correct tokens for correct email and password combination", async () => {

            userSelectorMock.expects("findUserByEmail")
                .withExactArgs(VALID_TEST_EMAIL)
                .resolves(EXISTING_USER);

            passwordHashingServiceMock.expects("verifyPassword")
                .withExactArgs(VALID_HASHED_PASSWORD, VALID_TEST_PASSWORD)
                .resolves(true);

            let expectedTokenResponse = new TokenResponse("the headers and payload", "the signature", null, EXISTING_USER.id);

            tokenGeneratorMock.expects("generateAuthToken")
                .withExactArgs(EXISTING_USER.id, EXISTING_USER.proxyId)
                .returns(expectedTokenResponse);

            await expect(UserLoginService.login(VALID_TEST_EMAIL, VALID_TEST_PASSWORD))
                .to.eventually.be.equal(expectedTokenResponse);
        });
    });

    after(() => {

        userSelectorMock.restore();
        tokenGeneratorMock.restore();
        passwordHashingServiceMock.restore();
    });
});
