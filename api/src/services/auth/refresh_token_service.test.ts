import chai = require("chai");
import sinon = require("sinon");
import JWTService = require("./tokens/jwt_service");
import TokenClaims = require("./tokens/token_claims");
import TokenGenerator = require("./tokens/token_generator");
import TokenResponse = require("./tokens/token_response");
import RefreshTokenService = require("./refresh_token_service");
import InvalidTokenError = require("../../shared/errors/types/specific/invalid_token_error");
import UserSelector = require("../../shared/database/selectors/user_selector");
import User = require("../../models/user");

let expect = chai.expect;

describe("RefreshTokenService class", () => {

    const EXISTING_USER: User = {
        id: "some_id",
        email: "example@test.com",
        hashedPassword: "wedjdadasdd",
        proxyId: "someTestProxyId",
        privilegeLevel: "user"
    };

    const VALID_PROXY_ID: string = "validProxyId";
    const INVALID_PROXY_ID: string = "invalidProxyId";
    const INVALID_USER_ID: string = "invalidUserId";

    const VALID_JWT_TOKEN = "thisIsA.nExampleJWTToke.nWhichIsValid";
    const INVALID_JWT_TOKEN = "thisIsAnE.xampleJWTToke.nButItsInvalid";

    let usersSelectorMock: sinon.SinonMock;
    let tokenGeneratorMock: sinon.SinonMock;
    let JWTServiceMock: sinon.SinonMock;

    before(async () => {

        usersSelectorMock = sinon.mock(UserSelector);
        tokenGeneratorMock = sinon.mock(TokenGenerator);
        JWTServiceMock = sinon.mock(JWTService);
    });

    describe("refreshAuthToken function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(RefreshTokenService.refreshAuthToken(null))
                .to.eventually.be.rejectedWith(RefreshTokenService.AUTHORISATION_MISSING_ERROR.reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(RefreshTokenService.refreshAuthToken(""))
                .to.eventually.be.rejectedWith(RefreshTokenService.AUTHORISATION_MISSING_ERROR.reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(RefreshTokenService.refreshAuthToken("        "))
                .to.eventually.be.rejectedWith(RefreshTokenService.AUTHORISATION_MISSING_ERROR.reason);
        });

        it("Exception correctly thrown when the token does not have three parts", async () => {

            await expect(RefreshTokenService.refreshAuthToken("a.b"))
                .to.eventually.be.rejectedWith(RefreshTokenService.AUTHORISATION_MISSING_ERROR.reason);
        });

        it("Exception correctly thrown when JWTService throws an error", async () => {

            JWTServiceMock.expects("verifyToken")
                .withExactArgs(INVALID_JWT_TOKEN)
                .throws(new InvalidTokenError());

            await expect(RefreshTokenService.refreshAuthToken(INVALID_JWT_TOKEN))
                .to.eventually.be.rejectedWith(new InvalidTokenError().reason);
        });

        it("Exception correctly thrown when proxy ID is not valid", async () => {

            JWTServiceMock.expects("verifyToken")
                .withExactArgs(VALID_JWT_TOKEN)
                .returns(new TokenClaims(null, INVALID_PROXY_ID, null, null, null, null));

            usersSelectorMock.expects("findUserByProxyId")
                .withExactArgs(INVALID_PROXY_ID)
                .resolves(null);

            await expect(RefreshTokenService.refreshAuthToken(VALID_JWT_TOKEN))
                .to.eventually.be.rejectedWith(new InvalidTokenError().reason);
        });

        it("Exception correctly thrown when proxy ID and user ID don't match up", async () => {

            JWTServiceMock.expects("verifyToken")
                .withExactArgs(VALID_JWT_TOKEN)
                .returns(new TokenClaims(INVALID_USER_ID, VALID_PROXY_ID, null, null, null, null));

            usersSelectorMock.expects("findUserByProxyId")
                .withExactArgs(VALID_PROXY_ID)
                .resolves(EXISTING_USER);

            await expect(RefreshTokenService.refreshAuthToken(VALID_JWT_TOKEN))
                .to.eventually.be.rejectedWith(new InvalidTokenError().reason);
        });

        it("New auth token correctly generated when all validation is passed", async () => {

            JWTServiceMock.expects("verifyToken")
                .withExactArgs(VALID_JWT_TOKEN)
                .returns(new TokenClaims(EXISTING_USER.id, VALID_PROXY_ID, null, null, null, null));

            usersSelectorMock.expects("findUserByProxyId")
                .withExactArgs(VALID_PROXY_ID)
                .resolves(EXISTING_USER);

            let expectedTokenResponse = new TokenResponse("some auth token", null, null, EXISTING_USER.id);

            tokenGeneratorMock.expects("generateAuthToken")
                .withExactArgs(EXISTING_USER.id, VALID_PROXY_ID)
                .returns(expectedTokenResponse);

            await expect(RefreshTokenService.refreshAuthToken(VALID_JWT_TOKEN))
                .to.eventually.be.equal(expectedTokenResponse);
        });
    });

    after(() => {

        usersSelectorMock.restore();
        tokenGeneratorMock.restore();
        JWTServiceMock.restore();
    });
});
