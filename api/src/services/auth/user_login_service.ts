import TokenResponse = require("./tokens/token_response");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import User = require("../../models/user");
import UserSelector = require("../../shared/database/selectors/user_selector");
import UnauthorisedError = require("../../shared/errors/types/base/unauthorised_error");
import PasswordHashingService = require("./password_hashing_service");
import TokenGenerator = require("./tokens/token_generator");

class UserLoginService {

    public static INCORRECT_EMAIL_PASSWORD_ERROR = new UnauthorisedError("incorrect_email_password", "Incorrect email and password combination");

    public static async login(email: string, givenPassword: string): Promise<TokenResponse> {

        this.checkForNullFields(email, givenPassword);

        let user: User = await UserSelector.findUserByEmail(email)
            .catch((error) => {

                throw error;
            });

        if (user == null) {

            throw this.INCORRECT_EMAIL_PASSWORD_ERROR;
        }

        let expectedHashedPassword: string = user.hashedPassword;

        let passwordIsCorrect: boolean = await PasswordHashingService.verifyPassword(expectedHashedPassword, givenPassword)
            .catch((error) => {

                throw error
            });

        if (!passwordIsCorrect) {

            throw this.INCORRECT_EMAIL_PASSWORD_ERROR;
        }

        return TokenGenerator.generateAuthToken(user.id, user.proxyId);
    }

    private static checkForNullFields(email: string, givenPassword: string): void {
        if (this.isEmpty(email) || this.isEmpty(givenPassword)) {
            throw new MissingFieldsError();
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = UserLoginService;
