import chai = require("chai");
import sinon = require("sinon");
import UserSelector = require("../../shared/database/selectors/user_selector");
import ProxyIdGenerator = require("./proxy_id_generator");
import InternalServerError = require("../../shared/errors/types/base/internal_server_error");
import User = require("../../models/user");

let expect = chai.expect;

describe("ProxyIdGenerator class", () => {

    const EXISTING_USER: User = {
        id: "some_id",
        email: "example@test.com",
        hashedPassword: "wedjdadasdd",
        proxyId: "someTestProxyId",
        privilegeLevel: "user"
    };

    let usersSelectorMock: sinon.SinonMock;

    before(async () => {

        usersSelectorMock = sinon.mock(UserSelector);
    });

    describe("generateNewProxyId function", () => {

        it("New ID generated if ID generated already belongs to a user", async () => {

            usersSelectorMock.expects("findUserByProxyId")
                .resolves(EXISTING_USER);
            usersSelectorMock.expects("findUserByProxyId")
                .resolves(null);

            await expect(ProxyIdGenerator.generateNewProxyId())
                .to.eventually.be.not.equal(null);
        });

        it("Exception correctly thrown for some unknown error", async () => {

            usersSelectorMock.expects("findUserByProxyId")
                .rejects(new InternalServerError());

            await expect(ProxyIdGenerator.generateNewProxyId())
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });
    });

    after(() => {

        usersSelectorMock.restore();
    });
});
