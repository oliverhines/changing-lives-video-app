import chai = require("chai");
import sinon = require("sinon");
import VideoCreationService = require("./video_creation_service");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import CourseSelector = require("../../shared/database/selectors/course_selector");
import Course = require("../../models/course");
import VideoSelector = require("../../shared/database/selectors/video_selector");
import Video = require("../../models/video");
import VideoHelper = require("./video_helper");

let expect = chai.expect;

describe("VideoCreationService class", () => {

    const VALID_TEST_COURSE_ID: string = "some-id";
    const VALID_TEST_TITLE: string = "some title";
    const VALID_TEST_DESCRIPTION: string = "some description";

    const INVALID_TEST_COURSE_ID: string = "doesnt exist";

    const EXISTING_COURSE: Course = {

        id: VALID_TEST_COURSE_ID,
        name: "Some name"
    };

    const TEST_UPLOAD_URL: string = "http://blaasdsadas";

    let courseSelectorMock: sinon.SinonMock;
    let videoSelectorMock: sinon.SinonMock;
    let videoHelperMock: sinon.SinonMock;

    before(async () => {

        courseSelectorMock = sinon.mock(CourseSelector);
        videoSelectorMock = sinon.mock(VideoSelector);
        videoHelperMock = sinon.mock(VideoHelper);
    });

    describe("createNewVideo function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(VideoCreationService.createNewVideo(null, VALID_TEST_TITLE, VALID_TEST_DESCRIPTION))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(VideoCreationService.createNewVideo(VALID_TEST_COURSE_ID, "", VALID_TEST_DESCRIPTION))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(VideoCreationService.createNewVideo(VALID_TEST_COURSE_ID, VALID_TEST_TITLE, "        "))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when the course doesn't exist", async () => {

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(INVALID_TEST_COURSE_ID)
                .resolves(null);

            await expect(VideoCreationService.createNewVideo(INVALID_TEST_COURSE_ID, VALID_TEST_TITLE, VALID_TEST_DESCRIPTION))
                .to.eventually.be.rejectedWith(VideoCreationService.COURSE_DOESNT_EXIST_ERROR.reason);
        });

        it("Exception correctly thrown when the title is too long", async () => {

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(VALID_TEST_COURSE_ID)
                .resolves(EXISTING_COURSE);

            await expect(VideoCreationService.createNewVideo(VALID_TEST_COURSE_ID, "cP0h9iSVaX7LQLJy2QLi3RfLfV22p5a6pp4cHWx8EAOje8ZR6LqaLgwaPtRcZdw07Q", VALID_TEST_DESCRIPTION))
                .to.eventually.be.rejectedWith(VideoCreationService.TITLE_LENGTH_ERROR.reason);
        });

        it("Exception correctly thrown when the description is too long", async () => {

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(VALID_TEST_COURSE_ID)
                .resolves(EXISTING_COURSE);

            await expect(VideoCreationService.createNewVideo(VALID_TEST_COURSE_ID, VALID_TEST_DESCRIPTION, "g6Deg7z4qYYsCNjTW8P71eNCEDrqrr7wZjayh1rqnuHTWnwAmsZeefvWs9yrQTmUkPY6z3tIL1hjvcJKBzuchfqphim61nVndC91eaFZs5RsNVVagSRKvh1ll1JistSwZ53FED5ZmRfsSaEIJePSmmVR75bYZ0gOKfgVBzWvKqB626xtcLnZGgVjgfsJRJUiSwgoQslmfneU7KD0OsO7GdWP4BPaYULyNnKbb4CPwegZ2ilYE1bOp4BTkviHLu0I"))
                .to.eventually.be.rejectedWith(VideoCreationService.DESCRIPTION_LENGTH_ERROR.reason);
        });

        it("Service correctly calls VideoURLGenerator for valid parameters", async () => {

            let newVideo: Video = {

                id: "some'id",
                courseId: VALID_TEST_COURSE_ID,
                title: VALID_TEST_TITLE,
                description: VALID_TEST_DESCRIPTION
            };

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(VALID_TEST_COURSE_ID)
                .resolves(EXISTING_COURSE);

            videoSelectorMock.expects("createNewVideo")
                .withExactArgs(VALID_TEST_COURSE_ID, VALID_TEST_TITLE, VALID_TEST_DESCRIPTION)
                .resolves(newVideo);

            videoHelperMock.expects("generateUploadURL")
                .withExactArgs(newVideo.id)
                .resolves(TEST_UPLOAD_URL);

            await expect(VideoCreationService.createNewVideo(VALID_TEST_COURSE_ID, VALID_TEST_TITLE, VALID_TEST_DESCRIPTION))
                .to.eventually.be.equal(TEST_UPLOAD_URL);
        });
    });

    after(() => {

        courseSelectorMock.restore();
        videoSelectorMock.restore();
        videoHelperMock.restore();
    });
});
