import chai = require("chai");
import sinon = require("sinon");
import VideoRetrievalService = require("./video_retrieval_service");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import VideoSelector = require("../../shared/database/selectors/video_selector");
import VideoHelper = require("./video_helper");
import Video = require("../../models/video");

let expect = chai.expect;

describe("VideoRetrievalService class", () => {

    const VALID_TEST_VIDEO_ID: string = "some-id";

    const INVALID_TEST_VIDEO_ID: string = "doesnt exist";

    const EXISTING_VIDEO: Video = {

        id: VALID_TEST_VIDEO_ID,
        title: "Some title",
        description: "Some description",
        courseId: "some-course-id"
    };

    let videoSelectorMock: sinon.SinonMock;
    let videoHelperMock: sinon.SinonMock;

    before(async () => {

        videoSelectorMock = sinon.mock(VideoSelector);
        videoHelperMock = sinon.mock(VideoHelper);
    });

    describe("getVideoById function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(VideoRetrievalService.getVideoById(null))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(VideoRetrievalService.getVideoById(""))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(VideoRetrievalService.getVideoById("        "))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when video is not in database", async () => {

            videoSelectorMock.expects("findVideoById")
                .withExactArgs(INVALID_TEST_VIDEO_ID)
                .resolves(null);

            await expect(VideoRetrievalService.getVideoById(INVALID_TEST_VIDEO_ID))
                .to.eventually.be.rejectedWith(VideoRetrievalService.VIDEO_DOESNT_EXIST_ERROR.reason);
        });

        it("Exception correctly thrown when video in database but not storage", async () => {

            videoSelectorMock.expects("findVideoById")
                .withExactArgs(INVALID_TEST_VIDEO_ID)
                .resolves(EXISTING_VIDEO);

            videoHelperMock.expects("videoExistsInStorage")
                .withExactArgs(INVALID_TEST_VIDEO_ID)
                .resolves(false);

            await expect(VideoRetrievalService.getVideoById(INVALID_TEST_VIDEO_ID))
                .to.eventually.be.rejectedWith(VideoRetrievalService.VIDEO_DOESNT_EXIST_ERROR.reason);
        });

        it("Service correctly calls VideoHelper for valid parameters", async () => {

            videoSelectorMock.expects("findVideoById")
                .twice()
                .withExactArgs(VALID_TEST_VIDEO_ID)
                .resolves(EXISTING_VIDEO);

            videoHelperMock.expects("videoExistsInStorage")
                .withExactArgs(VALID_TEST_VIDEO_ID)
                .resolves(true);

            await expect(VideoRetrievalService.getVideoById(VALID_TEST_VIDEO_ID))
                .to.eventually.be.equal(EXISTING_VIDEO);
        });
    });

    after(() => {

        videoSelectorMock.restore();
        videoHelperMock.restore();
    });
});
