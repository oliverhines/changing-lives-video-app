import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import Course = require("../../models/course");
import CourseSelector = require("../../shared/database/selectors/course_selector");
import ValidationError = require("../../shared/errors/types/specific/validation_error");
import Video = require("../../models/video");
import VideoSelector = require("../../shared/database/selectors/video_selector");
import VideoHelper from "./video_helper";

class VideoCreationService {

    public static COURSE_DOESNT_EXIST_ERROR: ValidationError = new ValidationError("courseId", "valid", "The specified course does not exist");
    public static TITLE_LENGTH_ERROR = new ValidationError("title", "length", "Title length must be no more than 64 characters");
    public static DESCRIPTION_LENGTH_ERROR = new ValidationError("description", "length", "Description length must be no more than 255 characters");

    public static async createNewVideo(courseId: string, title: string, description: string): Promise<string> {

        this.checkForNullFields(courseId, title, description);

        let courseExists: boolean = await this.courseExists(courseId);

        if (!courseExists) {

            throw this.COURSE_DOESNT_EXIST_ERROR;
        }

        if (!this.titleLengthValid(title)) {

            throw this.TITLE_LENGTH_ERROR;
        }

        if (!this.descriptionLengthValid(description)) {

            throw this.DESCRIPTION_LENGTH_ERROR;
        }

        let newVideo: Video = await VideoSelector.createNewVideo(courseId, title, description)
            .catch((error) => {

                throw error;
            });

        return await VideoHelper.generateUploadURL(newVideo.id)
            .catch((error) => {

                throw error;
            });
    }

    private static async courseExists(courseId: string): Promise<boolean> {

        let existingCourse: Course = await CourseSelector.findCourseById(courseId)
            .catch((error) => {

                throw error;
            });

        return existingCourse != null;
    }

    private static titleLengthValid(title: string): boolean {

        return title.length < 65;
    }

    private static descriptionLengthValid(description: string): boolean {

        return description.length < 256;
    }

    private static checkForNullFields(courseId: string, title: string, description: string): void {
        if (this.isEmpty(courseId) || this.isEmpty(title) || this.isEmpty(description)) {
            throw new MissingFieldsError();
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = VideoCreationService;
