import MissingFieldsError from "../../shared/errors/types/specific/missing_fields_error";
import Video from "../../models/video";
import VideoSelector from "../../shared/database/selectors/video_selector";
import ValidationError from "../../shared/errors/types/specific/validation_error";
import VideoHelper from "./video_helper";

class VideoDownloadService {

    public static VIDEO_DOESNT_EXIST_ERROR: ValidationError = new ValidationError("videoId", "valid", "The specified video does not exist");

    public static async getDownloadURL(videoId: string): Promise<string> {

        this.checkForNullFields(videoId);

        let videoExists: boolean = await this.videoExists(videoId);

        if (!videoExists) {

            throw this.VIDEO_DOESNT_EXIST_ERROR;
        }

        return await VideoHelper.generateDownloadURL(videoId)
            .catch((error) => {

                throw error;
            });
    }

    private static async videoExists(videoId: string): Promise<boolean> {

        // Check if video exists in database first
        let existingVideo: Video = await VideoSelector.findVideoById(videoId)
            .catch((error) => {

                throw error;
            });

        if (existingVideo == null) {

            return false;
        }

        // Now, check if video exists in storage as well
        return await VideoHelper.videoExistsInStorage(videoId)
            .catch((error) => {

                throw error;
            });
    }

    private static checkForNullFields(videoId): void {
        if (this.isEmpty(videoId)) {
            throw new MissingFieldsError();
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = VideoDownloadService;
