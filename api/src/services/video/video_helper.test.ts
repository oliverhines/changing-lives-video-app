import chai = require("chai");
import sinon = require("sinon");
import StorageHelper = require("../../shared/storage_helper");
import InternalServerError = require("../../shared/errors/types/base/internal_server_error");
import VideoHelper = require("./video_helper");

let expect = chai.expect;

describe("VideoHelper", () => {

    const TEST_ID: string = "test123";
    const TEST_URL: string = "http://blahblah";
    const TEST_ISSUED_AT_DATE = new Date(2000, 1, 1, 0, 0, 0, 0);
    const TEST_EXPIRES_AT_DATE = new Date(2000, 1, 1, 2, 0, 0, 0);

    let storageHelperMock: sinon.SinonMock;
    let dateMock: sinon.SinonMock;

    before(async () => {

        storageHelperMock = sinon.mock(StorageHelper);
        dateMock = sinon.mock(Date);
    });

    describe("generateUploadURL function", () => {

        it("Helper correctly calls storage helper with appropriate arguments", async () => {

            storageHelperMock.expects("getSignedUploadUrl")
                .withExactArgs("videos/" + TEST_ID + ".mp4", TEST_EXPIRES_AT_DATE)
                .resolves(TEST_URL);

            dateMock.expects("now")
                .returns(TEST_ISSUED_AT_DATE.getTime());

            await expect(VideoHelper.generateUploadURL(TEST_ID))
                .to.eventually.be.equal(TEST_URL);
        });

        it("Exception correctly thrown for some unknown error from storage helper", async () => {

            storageHelperMock.expects("getSignedUploadUrl")
                .withExactArgs("videos/" + TEST_ID + ".mp4", TEST_EXPIRES_AT_DATE)
                .rejects(new Error("some test error"));

            dateMock.expects("now")
                .returns(TEST_ISSUED_AT_DATE.getTime());

            await expect(VideoHelper.generateUploadURL(TEST_ID))
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });
    });

    describe("generateDownloadURL function", () => {

        it("Helper correctly calls storage helper with appropriate arguments", async () => {

            storageHelperMock.expects("getSignedDownloadUrl")
                .withExactArgs("videos/" + TEST_ID + ".mp4", TEST_EXPIRES_AT_DATE)
                .resolves(TEST_URL);

            dateMock.expects("now")
                .returns(TEST_ISSUED_AT_DATE.getTime());

            await expect(VideoHelper.generateDownloadURL(TEST_ID))
                .to.eventually.be.equal(TEST_URL);
        });

        it("Exception correctly thrown for some unknown error from storage helper", async () => {

            storageHelperMock.expects("getSignedDownloadUrl")
                .withExactArgs("videos/" + TEST_ID + ".mp4", TEST_EXPIRES_AT_DATE)
                .rejects(new Error("some test error"));

            dateMock.expects("now")
                .returns(TEST_ISSUED_AT_DATE.getTime());

            await expect(VideoHelper.generateDownloadURL(TEST_ID))
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });
    });

    describe("videoExistsInStorage function", () => {

        it("Helper correctly calls storage helper with appropriate arguments and returns appropriate value", async () => {

            storageHelperMock.expects("fileExists")
                .withExactArgs("videos/" + TEST_ID + ".mp4")
                .resolves(true);

            await expect(VideoHelper.videoExistsInStorage(TEST_ID))
                .to.eventually.be.equal(true);
        });

        it("Exception correctly thrown for some unknown error from storage helper", async () => {

            storageHelperMock.expects("fileExists")
                .withExactArgs("videos/" + TEST_ID + ".mp4")
                .rejects(new Error("some test error"));

            await expect(VideoHelper.videoExistsInStorage(TEST_ID))
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });
    });

    after(() => {

        storageHelperMock.restore();
        dateMock.restore();
    });
});
