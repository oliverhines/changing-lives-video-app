import StorageHelper from "../../shared/storage_helper";
import InternalServerError from "../../shared/errors/types/base/internal_server_error";

class VideoHelper {

    private static VIDEO_FOLDER_NAME = "videos";
    private static VIDEO_FILE_TYPE = "mp4";

    private static UPLOAD_HOURS_TO_EXPIRY = 2;
    private static DOWNLOAD_HOURS_TO_EXPIRY = 2;

    public static async generateUploadURL(videoId: string): Promise<string> {

        let filePathAndName: string = this.getVideoPathAndName(videoId);
        let expiryDate: Date = this.generateUploadExpiryDate();

        return await StorageHelper.getSignedUploadUrl(filePathAndName, expiryDate)
            .catch((error) => {

                throw new InternalServerError();
            });
    }

    public static async generateDownloadURL(videoId: string): Promise<string> {

        let filePathAndName: string = this.getVideoPathAndName(videoId);
        let expiryDate: Date = this.generateDownloadExpiryDate();

        return await StorageHelper.getSignedDownloadUrl(filePathAndName, expiryDate)
            .catch((error) => {

                throw new InternalServerError();
            })
    }

    public static async videoExistsInStorage(videoId: string): Promise<boolean> {

        let filePathAndName: string = this.getVideoPathAndName(videoId);

        return await StorageHelper.fileExists(filePathAndName)
            .catch((error) => {

                throw new InternalServerError();
            });
    }

    private static getVideoPathAndName(videoId: string): string {

        return this.VIDEO_FOLDER_NAME + "/" + videoId + "." + this.VIDEO_FILE_TYPE;
    }

    private static generateUploadExpiryDate(): Date {

        let expiryDate: Date = new Date(Date.now());
        expiryDate.setHours(expiryDate.getHours() + this.UPLOAD_HOURS_TO_EXPIRY);

        return expiryDate;
    }

    private static generateDownloadExpiryDate(): Date {

        let expiryDate: Date = new Date(Date.now());
        expiryDate.setHours(expiryDate.getHours() + this.DOWNLOAD_HOURS_TO_EXPIRY);

        return expiryDate;
    }
}

export = VideoHelper;
