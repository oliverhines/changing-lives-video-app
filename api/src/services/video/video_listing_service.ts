import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import Course = require("../../models/course");
import CourseSelector = require("../../shared/database/selectors/course_selector");
import ValidationError = require("../../shared/errors/types/specific/validation_error");
import Video = require("../../models/video");
import VideoSelector = require("../../shared/database/selectors/video_selector");

class VideoListingService {

    public static COURSE_DOESNT_EXIST_ERROR: ValidationError = new ValidationError("courseId", "valid", "The specified course does not exist");

    public static async listVideosForCourse(courseId: string): Promise<Video[]> {

        this.checkForNullFields(courseId);

        let courseExists: boolean = await this.courseExists(courseId);

        if (!courseExists){
            throw this.COURSE_DOESNT_EXIST_ERROR;
        }

        return await VideoSelector.findVideosByCourseId(courseId)
            .catch((error) => {

                throw error;
            });
    }

    private static checkForNullFields(courseId: string): void {
        if (this.isEmpty(courseId)) {
            throw new MissingFieldsError();
        }
    }

    private static async courseExists(courseId: string): Promise<boolean> {

        let existingCourse: Course = await CourseSelector.findCourseById(courseId)
            .catch((error) => {

                throw error;
            });

        return existingCourse != null;
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = VideoListingService;
