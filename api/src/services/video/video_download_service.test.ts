import chai = require("chai");
import sinon = require("sinon");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import VideoSelector = require("../../shared/database/selectors/video_selector");
import VideoDownloadService = require("./video_download_service");
import Video = require("../../models/video");
import VideoHelper = require("./video_helper");

let expect = chai.expect;

describe("VideoDownloadService class", () => {

    const VALID_TEST_VIDEO_ID: string = "some-id";

    const INVALID_TEST_VIDEO_ID: string = "doesnt exist";

    const EXISTING_VIDEO: Video = {

        id: VALID_TEST_VIDEO_ID,
        title: "Some title",
        description: "Some description",
        courseId: "some-course-id"
    };

    let videoSelectorMock: sinon.SinonMock;
    let videoHelperMock: sinon.SinonMock;

    before(async () => {

        videoSelectorMock = sinon.mock(VideoSelector);
        videoHelperMock = sinon.mock(VideoHelper);
    });

    describe("getDownloadURL function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(VideoDownloadService.getDownloadURL(null))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(VideoDownloadService.getDownloadURL(""))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(VideoDownloadService.getDownloadURL("        "))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when video is not in database", async () => {

            videoSelectorMock.expects("findVideoById")
                .withExactArgs(INVALID_TEST_VIDEO_ID)
                .resolves(null);

            await expect(VideoDownloadService.getDownloadURL(INVALID_TEST_VIDEO_ID))
                .to.eventually.be.rejectedWith(VideoDownloadService.VIDEO_DOESNT_EXIST_ERROR.reason);
        });

        it("Exception correctly thrown when video in database but not storage", async () => {

            videoSelectorMock.expects("findVideoById")
                .withExactArgs(INVALID_TEST_VIDEO_ID)
                .resolves(EXISTING_VIDEO);

            videoHelperMock.expects("videoExistsInStorage")
                .withExactArgs(INVALID_TEST_VIDEO_ID)
                .resolves(false);

            await expect(VideoDownloadService.getDownloadURL(INVALID_TEST_VIDEO_ID))
                .to.eventually.be.rejectedWith(VideoDownloadService.VIDEO_DOESNT_EXIST_ERROR.reason);
        });

        it("Service correctly calls VideoHelper for valid parameters", async () => {

            let downloadURL: string = "http://blahblah";

            videoSelectorMock.expects("findVideoById")
                .withExactArgs(VALID_TEST_VIDEO_ID)
                .resolves(EXISTING_VIDEO);

            videoHelperMock.expects("videoExistsInStorage")
                .withExactArgs(VALID_TEST_VIDEO_ID)
                .resolves(true);

            videoHelperMock.expects("generateDownloadURL")
                .withExactArgs(VALID_TEST_VIDEO_ID)
                .resolves(downloadURL);

            await expect(VideoDownloadService.getDownloadURL(VALID_TEST_VIDEO_ID))
                .to.eventually.be.equal(downloadURL);
        });
    });

    after(() => {

        videoSelectorMock.restore();
        videoHelperMock.restore();
    });
});
