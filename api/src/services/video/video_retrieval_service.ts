import Video from "../../models/video";
import MissingFieldsError from "../../shared/errors/types/specific/missing_fields_error";
import VideoSelector from "../../shared/database/selectors/video_selector";
import VideoHelper from "./video_helper";
import ValidationError from "../../shared/errors/types/specific/validation_error";

class VideoRetrievalService {

    public static VIDEO_DOESNT_EXIST_ERROR: ValidationError = new ValidationError("videoId", "valid", "The specified video does not exist");

    public static async getVideoById(videoId: string): Promise<Video> {

        this.checkForNullFields(videoId);

        let videoExists: boolean = await this.videoExists(videoId);

        if (!videoExists) {

            throw this.VIDEO_DOESNT_EXIST_ERROR;
        }

        return await VideoSelector.findVideoById(videoId)
            .catch((error) => {

                throw error;
            });
    }

    private static async videoExists(videoId: string): Promise<boolean> {

        // Check if video exists in database first
        let existingVideo: Video = await VideoSelector.findVideoById(videoId)
            .catch((error) => {

                throw error;
            });

        if (existingVideo == null) {

            return false;
        }

        // Now, check if video exists in storage as well
        return await VideoHelper.videoExistsInStorage(videoId)
            .catch((error) => {

                throw error;
            });
    }

    private static checkForNullFields(id): void {
        if (this.isEmpty(id)) {
            throw new MissingFieldsError();
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = VideoRetrievalService;
