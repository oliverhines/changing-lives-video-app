import chai = require("chai");
import sinon = require("sinon");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import VideoListingService = require("./video_listing_service");
import VideoSelector = require("../../shared/database/selectors/video_selector");
import CourseSelector = require("../../shared/database/selectors/course_selector");
import Course = require("../../models/course");
import Video = require("../../models/video");

let expect = chai.expect;

describe("VideoListingService class", () => {

    const VALID_TEST_COURSE_ID: string = "some-id";

    const INVALID_TEST_COURSE_ID: string = "doesnt exist";

    const EXISTING_COURSE: Course = {
       id: VALID_TEST_COURSE_ID,
       name: "a course name"
    };

    const EXISTING_VIDEO: Video = {

        id: "1234",
        title: "Some title",
        description: "Some description",
        courseId: "some-course-id"
    };

    const VIDEOS: Video[] = [EXISTING_VIDEO];
    
    let videoSelectorMock: sinon.SinonMock;
    let courseSelectorMock: sinon.SinonMock;

    before(async () => {

        videoSelectorMock = sinon.mock(VideoSelector);
        courseSelectorMock = sinon.mock(CourseSelector);
    });

    describe("listVideosForCourse function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(VideoListingService.listVideosForCourse(null))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(VideoListingService.listVideosForCourse(""))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(VideoListingService.listVideosForCourse("        "))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when course is not in database", async () => {

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(INVALID_TEST_COURSE_ID)
                .resolves(null);

            await expect(VideoListingService.listVideosForCourse(INVALID_TEST_COURSE_ID))
                .to.eventually.be.rejectedWith(VideoListingService.COURSE_DOESNT_EXIST_ERROR.reason);
        });

        it("Service returns data for valid input", async () => {

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(VALID_TEST_COURSE_ID)
                .resolves(EXISTING_COURSE);
            videoSelectorMock.expects("findVideosByCourseId")
                .withExactArgs(VALID_TEST_COURSE_ID)
                .resolves(VIDEOS);

            await expect(VideoListingService.listVideosForCourse(VALID_TEST_COURSE_ID))
                .to.eventually.be.equal(VIDEOS);
        });
    });

    after(() => {

        videoSelectorMock.restore();
        courseSelectorMock.restore();
    });
});
