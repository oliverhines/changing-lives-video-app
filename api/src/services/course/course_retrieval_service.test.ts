import chai = require("chai");
import sinon = require("sinon");
import CourseSelector = require("../../shared/database/selectors/course_selector");
import CourseRetrievalService = require("./course_retrieval_service");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import Course = require("../../models/course");

let expect = chai.expect;

describe("CourseRetrievalService", () => {

    const VALID_TEST_ID: string = "valid-id";
    const INVALID_TEST_ID: string = "valid-id";

    const EXISTING_COURSE: Course = {

        id: VALID_TEST_ID,
        name: "Some random name"
    };

    let courseSelectorMock: sinon.SinonMock;

    before(async () => {

        courseSelectorMock = sinon.mock(CourseSelector);
    });

    describe("getCourseById function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(CourseRetrievalService.getCourseById(null))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(CourseRetrievalService.getCourseById(""))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(CourseRetrievalService.getCourseById("        "))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Service throws correct error for non-existent course", async () => {

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(INVALID_TEST_ID)
                .resolves(null);

            await expect(CourseRetrievalService.getCourseById(INVALID_TEST_ID))
                .to.eventually.be.rejectedWith(CourseRetrievalService.COURSE_DOESNT_EXIST_ERROR);
        });

        it("Service returns correct data for valid course", async () => {

            courseSelectorMock.expects("findCourseById")
                .withExactArgs(VALID_TEST_ID)
                .resolves(EXISTING_COURSE);

            await expect(CourseRetrievalService.getCourseById(VALID_TEST_ID))
                .to.eventually.be.equal(EXISTING_COURSE);
        });
    });

    after(() => {

        courseSelectorMock.restore();
    });
});
