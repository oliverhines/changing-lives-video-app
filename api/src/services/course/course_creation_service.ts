import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import CourseSelector = require("../../shared/database/selectors/course_selector");
import ValidationError = require("../../shared/errors/types/specific/validation_error");

class CourseCreationService {

    public static NAME_LENGTH_ERROR = new ValidationError("name", "length", "Name length must be between 4 and 64 characters");

    public static async createNewCourse(name: string): Promise<void> {

        this.checkForNullFields(name);

        if (!this.nameLengthValid(name)) {

            throw this.NAME_LENGTH_ERROR;
        }

        await CourseSelector.createNewCourse(name)
            .catch((error) => {

                throw error;
            });

        return;
    }

    private static nameLengthValid(name: string): boolean {

        return name.length > 3 && name.length < 65;
    }

    private static checkForNullFields(name): void {
        if (this.isEmpty(name)) {

            throw new MissingFieldsError();
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = CourseCreationService;
