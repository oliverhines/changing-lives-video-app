import chai = require("chai");
import sinon = require("sinon");
import CourseSelector = require("../../shared/database/selectors/course_selector");
import CourseListingService = require("./course_listing_service");
import Course = require("../../models/course");
import InternalServerError = require("../../shared/errors/types/base/internal_server_error");

let expect = chai.expect;

describe("CourseListingService", () => {

    const EXAMPLE_COURSES: Course[] = [
        {
            id: "example id",
            name: "Some course"
        },
        {
            id: "another id",
            name: "Another course"
        }
    ];

    let courseSelectorMock: sinon.SinonMock;

    before(async () => {

        courseSelectorMock = sinon.mock(CourseSelector);
    });

    describe("listCourses function", () => {

        it("Service correctly makes call to selector and returns information", async () => {

            courseSelectorMock.expects("getCourses")
                .resolves(EXAMPLE_COURSES);


            await expect(CourseListingService.listCourses())
                .to.eventually.be.equal(EXAMPLE_COURSES);
        });

        it("Exception correctly thrown for some selector error", async () => {

            courseSelectorMock.expects("getCourses")
                .rejects(new InternalServerError());


            await expect(CourseListingService.listCourses())
                .to.eventually.be.rejectedWith(new InternalServerError().reason);
        });
    });

    after(() => {

        courseSelectorMock.restore();
    });
});
