import MissingFieldsError from "../../shared/errors/types/specific/missing_fields_error";
import Course from "../../models/course";
import CourseSelector from "../../shared/database/selectors/course_selector";
import ValidationError from "../../shared/errors/types/specific/validation_error";

class CourseRetrievalService {

    public static COURSE_DOESNT_EXIST_ERROR: ValidationError = new ValidationError("courseId", "valid", "The specified course does not exist");

    public static async getCourseById(courseId: string): Promise<Course> {

        this.checkForNullFields(courseId);

        let course: Course = await CourseSelector.findCourseById(courseId)
            .catch((error) => {

                throw error;
            });

        if (course == null) {

            throw this.COURSE_DOESNT_EXIST_ERROR;
        }

        return course;
    }

    private static checkForNullFields(id): void {
        if (this.isEmpty(id)) {
            throw new MissingFieldsError();
        }
    }

    private static isEmpty(value) {
        return typeof value === "string" && !value.trim() || typeof value === "undefined" || value === null;
    }
}

export = CourseRetrievalService;
