import chai = require("chai");
import sinon = require("sinon");
import CourseCreationService = require("./course_creation_service");
import MissingFieldsError = require("../../shared/errors/types/specific/missing_fields_error");
import CourseSelector = require("../../shared/database/selectors/course_selector");

let expect = chai.expect;

describe("CourseCreationService", () => {

    let courseSelectorMock: sinon.SinonMock;

    before(async () => {

        courseSelectorMock = sinon.mock(CourseSelector);
    });

    describe("createNewCourse function", () => {

        it("Exception correctly thrown when there is a null field", async () => {

            await expect(CourseCreationService.createNewCourse(null))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is an empty field", async () => {

            await expect(CourseCreationService.createNewCourse(""))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when there is a whitespace field", async () => {

            await expect(CourseCreationService.createNewCourse("        "))
                .to.eventually.be.rejectedWith(new MissingFieldsError().reason);
        });

        it("Exception correctly thrown when name is too short", async () => {

            await expect(CourseCreationService.createNewCourse("123"))
                .to.eventually.be.rejectedWith(CourseCreationService.NAME_LENGTH_ERROR.reason);
        });

        it("Exception correctly thrown when name is too long", async () => {

            await expect(CourseCreationService.createNewCourse("12345678901234567890123456789012345678901234567890123456789012345"))
                .to.eventually.be.rejectedWith(CourseCreationService.NAME_LENGTH_ERROR.reason);
        });

        it("Course created successfully for some valid input", async () => {

            courseSelectorMock.expects("createNewCourse")
                .withExactArgs("example")
                .resolves(undefined);

            await expect(CourseCreationService.createNewCourse("example"))
                .to.eventually.be.equal(undefined);
        });
    });

    after(() => {

        courseSelectorMock.restore();
    });
});
