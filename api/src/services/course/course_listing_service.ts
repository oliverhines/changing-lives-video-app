import Course = require("../../models/course");
import CourseSelector = require("../../shared/database/selectors/course_selector");

class CourseListingService {

    public static async listCourses(): Promise<Course[]> {

        return await CourseSelector.getCourses()
            .catch((error) => {

                throw error;
            });
    }
}

export = CourseListingService;
