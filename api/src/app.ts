import express = require("express");
import bodyParser = require("body-parser");
import courseRouter = require("./routes/course");
import ErrorHandler = require("./shared/errors/error_handler");
import authRouter = require("./routes/auth");
import cookieParser = require("cookie-parser");
import videoRouter from "./routes/video";
import cors from "cors";

const app = express();

app.use(cookieParser());

app.use(cors());

app.use(bodyParser.urlencoded({
    extended: true,
}));

app.use("/auth", authRouter);
app.use("/course", courseRouter);
app.use("/video", videoRouter);

app.use(ErrorHandler.handleError);

export = app;
