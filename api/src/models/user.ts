interface User {

    id: string;
    email: string;
    hashedPassword: string;
    proxyId: string;
    privilegeLevel: string;
}

export = User;
