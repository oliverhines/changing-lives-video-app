import * as http from "http";
import app = require("./app");
import Database = require("./shared/database/database");
import Config from "./config";

try {
    Config.getSharedInstance().checkAllVariablesSet();
} catch (e) {

    console.error("Error: " + e.message);
    process.exit(1);
}

const server = http.createServer(app);

server.on("error", (error) => {

    // TODO Print error properly
    console.log("Could not start server");
    console.log(error);
    process.exit(1);
    return;
});

console.log("Initialising database");

Database.init()
    .then(() => {

        server.listen(8080, () => {

            console.log("Server listening on 3000");
        });
    })
    .catch((error) => {

        // TODO Handle error properly
        // TODO Make sure error is properly thrown from database
        console.error(error);
        process.exit(1);
    });
