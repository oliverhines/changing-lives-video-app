import express = require("express");
import CourseController = require("../controllers/course");

const courseRouter: express.Router = express.Router();

// TODO Require admin privileges
courseRouter.post("/create", CourseController.createCourse);
courseRouter.get("/list", CourseController.listCourses);
courseRouter.get("/", CourseController.getCourse);

export = courseRouter;
