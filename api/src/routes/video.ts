import express = require("express");
import VideoController from "../controllers/video";

const videoRouter: express.Router = express.Router();

// TODO Require admin privileges
videoRouter.post("/create", VideoController.createVideo);
// TODO Require authentication
videoRouter.get("/download", VideoController.getDownloadURL);
videoRouter.get("/", VideoController.getVideo);
videoRouter.get("/list", VideoController.listVideosForCourse);

export = videoRouter;
