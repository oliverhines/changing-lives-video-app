import express = require("express");
import AuthController = require("../controllers/auth");

const authRouter: express.Router = express.Router();

authRouter.post("/register", AuthController.registerNewUser);
authRouter.post("/login", AuthController.login);

export = authRouter;
