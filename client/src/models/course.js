class Course {

    constructor(id: string, name: string) {

        this.id = id;
        this.name = name;
    }
}

export default Course;
