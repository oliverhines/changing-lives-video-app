class Video {

    constructor(id: string, courseId: string, title: string, description: string) {

        this.id = id;
        this.courseId = courseId;
        this.title = title;
        this.description = description;
    }
}

export default Video;
