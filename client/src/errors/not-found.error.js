import APIError from "./api-error";

class NotFoundError extends APIError {

    constructor() {
        let reason: string = "The requested resource could not be found";
        super(404, "Not Found", "not_found", reason, reason);
    }
}

export default NotFoundError;
