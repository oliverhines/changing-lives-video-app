import APIError from "./api-error";

class NetworkError extends APIError {

    constructor() {
        super(null, "Network Error", "network_error",
            "Could not connect to the server",
            "Could not connect to the server, please check your internet connection and try again.");
    }
}

export default NetworkError;
