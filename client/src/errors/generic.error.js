import APIError from "./api-error";

class GenericError extends APIError {

    constructor() {
        super(500, "Internal Server Error", "internal_server",
            "An internal server error occurred, please try again later",
            "An error occurred, please try again later");
    }
}

export default GenericError;
