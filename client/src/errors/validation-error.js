import APIError from "./api-error";

class ValidationError extends APIError {

    constructor(field: string, constraint: string, reason: string) {
        super(422, "Validation Error", "validation", reason, reason);
    }
}

export default ValidationError;
