class APIError {

    constructor(statusCode: number, type: string, subType: string, reason: string, message: string) {

        this.statusCode = statusCode;
        this.type = type;
        this.subType = type;
        this.reason = reason;
        this.message = message;
    }
}

export default APIError;
