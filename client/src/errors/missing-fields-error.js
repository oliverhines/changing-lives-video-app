import APIError from "./api-error";

class MissingFieldsError extends APIError {

    constructor() {
        let reason: string = "There were missing fields in the request.";
        super(422, "Missing Fields Error", "missing_fields", reason, reason);
    }
}

export default MissingFieldsError;
