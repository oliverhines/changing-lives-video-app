import axios, {AxiosResponse} from "axios";
import APIError from "../errors/api-error";
import NotFoundError from "../errors/not-found.error";
import GenericError from "../errors/generic.error";
import NetworkError from "../errors/network-error";
import MissingFieldsError from "../errors/missing-fields-error";
import ValidationError from "../errors/validation-error";

class APIService {

    static BASE_ADDRESS: string = 'http://localhost:8080';

    static async performGetRequest(endpointAddress: string, params: {} = {}): Promise<any> {

        let address: string = APIService.BASE_ADDRESS + endpointAddress;

        return await axios.get(address, { params: params})
            .then((response) => {

                return response.data;
            })
            .catch((error) => {

                throw this.parseError(error);
            });
    }

    static async performPostRequest(endpointAddress: string, data: {} = {}): Promise<any> {

        let address: string = APIService.BASE_ADDRESS + endpointAddress;

        return await axios.post(address, data, {
            headers: {
                "Content-Type" : "application/x-www-form-urlencoded"
            },
            transformRequest: APIService.getQueryString
            })
            .then((response) => {

                return response.data;
            })
            .catch((error) => {

                throw this.parseError(error);
            })
    }

    static getQueryString(data = {}) {
        /**
         * This method is used to convert parameters into a query string in format a=b&c=d with correct character
         * encodings
         */
        return Object.entries(data)
            .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
            .join('&');
    }

    static parseError(error): APIError {

        // Check if we are not able to get a response from the server
        if (!error.response) {

            return new NetworkError();
        }

        // If we did get a response, then parse the appropriate error
        let response: AxiosResponse = error.response;
        let status: number = response.status;
        
        switch (status) {

            case 404:
                return new NotFoundError();
            case 422:

                // Check to see if we have a sub-type for our error so we can identify it
                if (!response.data.subType) {
                    // If we don't then just throw a generic error as we don't know what it is
                    return new GenericError();
                }

                let subType: string = response.data.subType;

                switch (subType) {
                    case "missing_fields":
                        return new MissingFieldsError();
                    case "validation":

                        let field: string = response.data.field;
                        let constraint: string = response.data.constraint;
                        let reason: string = response.data.reason;

                        return new ValidationError(field, constraint, reason);
                    default:
                        return new GenericError();
                }
            case 500:
                return new GenericError();
            default:
                return new GenericError();
        }
    }
}

export default APIService;
