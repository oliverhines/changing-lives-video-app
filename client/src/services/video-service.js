import APIService from "./api-service";
import axios from "axios";
import GenericError from "../errors/generic.error";
import Video from "../models/video";

class VideoService {

    static async getVideo(videoId: string): Video {

        let data = await APIService.performGetRequest("/video", {videoId: videoId})
            .catch((error) => {

                throw error;
            });

        return new Video(data.id, data.courseId, data.title, data.description);
    }

    static async getDownloadURL(videoId: string): string {

        let data = await APIService.performGetRequest("/video/download", {videoId: videoId})
            .catch((error) => {

                throw error;
            });

        return data.downloadURL;
    }

    static async listVideosForCourse(courseId: string): Video[] {

        let data = await APIService.performGetRequest("/video/list", {courseId: courseId})
            .catch((error) => {
                throw error;
            });

        let videos: Video[] = [];

        data.forEach(function (item, index) {


            videos.push(new Video(item.id, item.courseId, item.title, item.description));
        });

        return videos;
    }

    static async uploadVideo(title: string, description: string, courseId: string, videoFile): Promise<void> {

        let data = await APIService.performPostRequest("/video/create", {
            title: title,
            description: description,
            courseId: courseId})
            .catch((error) => {

                throw error;
            });

        let uploadURL: string = data.uploadURL;

        return await this.uploadToGoogleCloudStorage(uploadURL, videoFile)
            .catch((error) => {

                throw error;
            });
    }

    static async uploadToGoogleCloudStorage(uploadURL: string, file): Promise<void> {

        return await axios.put(uploadURL, file)
            .then((response) => {

                return null;
            })
            .catch((error) => {

                throw new GenericError();
            });
    }
}

export default VideoService;
