import Course from "../models/course";
import APIService from "./api-service";

class CourseService {

    static async listCourses(): Promise<Course[]> {

        let data = await APIService.performGetRequest("/course/list")
            .catch((error) => {

                throw error;
            });

        let courses: Course[] = [];

        data.forEach(function (item, index) {

            courses.push(new Course(item.id, item.name));
        });

        return courses;
    }

    static async createCourse(name: string): Promise<null> {

        await APIService.performPostRequest("/course/create", {
            name: name
        })
            .catch((error) => {

                throw error;
            });
    }

    static async getCourseById(id: string): Promise<Course> {

        let data = await APIService.performGetRequest("/course/", {courseId: id})
            .catch((error) => {

                throw error;
            });

        return new Course(data.id, data.name);
    }
}

export default CourseService;
