import React from 'react';
import {Link} from "react-router-dom";
import "./video-card.styles.css";

class VideoCard extends React.Component {

    render() {
        return (
            <div className="card text-white video-card">
                <img src={this.props.thumbnail} className="card-img-top" alt="Loading..."/>
                <div className="card-body video-card-body">
                    <h5 className="card-title">
                        {this.props.title}
                    </h5>
                    <p className="card-text">
                        {this.props.description}
                    </p>
                    <Link to={"/video/" + this.props.videoId}>
                        <button className="btn btn-outline-light" type="submit">
                            Watch
                        </button>
                    </Link>
                </div>
            </div>
        )
    }

}

export default VideoCard;
