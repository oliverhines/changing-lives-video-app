import React from 'react';
import VideoService from "../../services/video-service";
import VideoCard from "./video-card/video-card.component";
import placeholder from "../../Images/test.jpg";
import LoadingSpinner from "../shared/loading-spinner/loading-spinner.component";
import "./course-home.styles.css";
import CourseService from "../../services/course-service";

class CourseHome extends React.Component {

    state = {

        isFetching: false,
        videos: [],
        courseId: null,
        course: null
    };

    async componentDidMount(): void {

        await this.setState({...this.state, courseId: this.props.match.params.id});

        this.fetchPageContent()
            .catch((error) => {

                alert(error.message);
            });
    }

    async fetchPageContent(): Promise<void> {

        this.setState({...this.state, isFetching: true});

        let videos = await VideoService.listVideosForCourse(this.state.courseId)
            .catch((error) => {

                this.setState({...this.state, isFetching: false});
                throw error;
            });

        let course = await CourseService.getCourseById(this.state.courseId)
            .catch((error) => {

                this.setState({...this.state, isFetching: false});
                throw error;
            });

        this.setState({...this.state, isFetching: false, videos: videos, course: course});
    }

    render() {
        return (
            <div>
                {!this.state.isFetching &&
                <div>
                    <div>
                        {this.state.course != null &&
                        <div>
                            <h2 className="text-center">
                                {this.state.course.name}
                            </h2>
                            <hr />
                        </div>
                        }
                        <div className="nav justify-content-center">
                         {this.state.videos.map(video =>
                             <VideoCard title={video.title} description={video.description} thumbnail={placeholder} videoId={video.id}/>
                         )}
                        </div>
                    </div>
                </div>
                }
                {this.state.isFetching &&
                <div className="text-center course-home-spinner-container">
                    <LoadingSpinner/>
                </div>
                }
            </div>
        );
    }
}

export default CourseHome;
