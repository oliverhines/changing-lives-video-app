import React from 'react';
import VideoService from "../../services/video-service";
import CourseService from "../../services/course-service";
import LoadingSpinner from "../shared/loading-spinner/loading-spinner.component";

class UploadHome extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            title: "",
            description: "",
            courseId: null,
            isUploading: false,
            courses: []
        };

        this.videoFileInput = React.createRef();

        this.updateTitle = this.updateTitle.bind(this);
        this.updateDescription = this.updateDescription.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
        this.updateCourse = this.updateCourse.bind(this);
    }

    componentDidMount() {

        CourseService.listCourses()
            .then((courses) => {

                this.setState({...this.state, courses: courses});
            })
            .catch((error) => {

                alert("Couldn't load courses. Please try again later.");
            });
    }

    handleUpload(event) {
        event.preventDefault();

        let videoFile = this.videoFileInput.current.files[0];

        this.setState({...this.state, isUploading: true});

        VideoService.uploadVideo(this.state.title, this.state.description, this.state.courseId, videoFile)
            .then(() => {
                this.setState({...this.state, isUploading: false});
                alert("Video uploaded successfully");
            })
            .catch((error) => {
                this.setState({...this.state, isUploading: false});
                alert(error.message);
            });
    }

    updateTitle(event) {

        this.setState({...this.state, title: event.target.value});
    }

    updateDescription(event) {

        this.setState({...this.state, description: event.target.value});
    }

    updateCourse(event) {

        this.setState({...this.state, courseId: event.target.value});
    }

    render() {
        return (
            <div className="container-fluid" style={{padding: "10px"}}>
                <div className="row">
                    <div className="col-3">

                    </div>
                    <div className="col-6">
                        <h1>
                            Upload New Video:
                        </h1>
                        <form onSubmit={this.handleUpload}>
                            <div className="form-group row">
                                <label htmlFor="titleInput"
                                       className="col-3 col-form-label col-form-label-lg">
                                    Title:
                                </label>
                                <div className="col">
                                    <input type="text" className="form-control form-control-lg" id="titleInput"
                                           placeholder="Enter a title for your video..." value={this.state.title}
                                           onChange={this.updateTitle}/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="row">
                                    <label htmlFor="descriptionInput"
                                           className="col col-form-label col-form-label-lg">
                                        Description:
                                    </label>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <textarea className="form-control" id="descriptionInput"
                                                  rows="3" value={this.state.description}
                                                  onChange={this.updateDescription}
                                                  placeholder="Enter a description for your video...">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col">
                                    <select className="custom-select" onChange={this.updateCourse}>
                                        <option selected>Choose course...</option>
                                        {this.state.courses.map((course) => {
                                            return <option value={course.id}>{course.name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-9">
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="videoInput"
                                               ref={this.videoFileInput}/>
                                        <label className="custom-file-label" htmlFor="videoInput">
                                            Choose video file
                                        </label>
                                    </div>
                                </div>
                                <div className="col-3">
                                    <button type="submit" className="btn btn-primary" style={{width: "100%"}}>
                                        Upload
                                    </button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-5"/>
                                <div className="col-2">
                                    {this.state.isUploading && <LoadingSpinner />}
                                </div>
                                <div className="col-5"/>
                            </div>
                        </form>
                    </div>
                    <div className="col-3">
                    </div>
                </div>
            </div>
        );
    }
}

export default UploadHome;
