import React from 'react';

class LoadingSpinner extends React.Component {


    render() {
        return (
            <div className="spinner-border"/>
            );
    }
}

export default LoadingSpinner;
