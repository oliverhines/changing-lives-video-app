import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <footer id="sticky-footer" class="py-4 text-white-50" style = {{background: '#303572'}}>
                <div class="container text-center">
                    <small>Here's a footer</small>
                </div>
            </footer>
        )
    }
}

export default Footer
