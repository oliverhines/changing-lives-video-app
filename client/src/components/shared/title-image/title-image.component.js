import React from 'react';
import titleimg from "../../../Images/titleimg.jpg";
import "./title-image.styles.css";

class TitleImage extends React.Component {
    render() {
        return (
            <img src={titleimg} className="img-fluid title-image"/>
        )
    }
}

export default TitleImage;
