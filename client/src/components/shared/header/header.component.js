import React from 'react';
import {Link} from "react-router-dom";

class Header extends React.Component {
    render() {
        return (
            <nav class="navbar navbar-expand-lg" style = {{background: '#303572'}}>
                <div class="collapse navbar-collapse" id="navbarNav">

                    <ul class="navbar-nav" style = {{color: '#FFFFFF'}}>

                        <Link to={"/"}>
                            <li className="nav-item">
                                <a className="nav-link text-light" href="/">
                                    Home
                                    <span className="sr-only">(current)</span></a>
                            </li>
                        </Link>
                        <Link to={"/admin"}>
                            <li className="nav-item">
                                <a className="nav-link text-light" href="/admin">
                                    Admin
                                    <span className="sr-only">(current)</span></a>
                            </li>
                        </Link>
                        <Link to={"/upload"}>
                            <li className="nav-item">
                                <a className="nav-link text-light" href="/upload">
                                    Upload
                                    <span className="sr-only">(current)</span></a>
                            </li>
                        </Link>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default Header
