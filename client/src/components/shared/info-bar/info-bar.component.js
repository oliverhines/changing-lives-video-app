import React from 'react';
import logo from "../../../Images/logo.png";

class InfoBar extends React.Component {
    render() {
        return (
            <div class="pos-f-t">
                <nav class="navbar navbar-light bg-light">
                    <a class="navbar-brand" href="#">
                        <img src = {logo} style={{ height: 30, width: 'auto'}} class="d-inline-block align-top" alt=""></img>
                    </a>

                    <a class = "navbar-brand" href = "#">
                        <i style = {{color: '#CB5A9B'}} class="fa fa-envelope-o" aria-hidden="true">  </i>
                        &nbsp; central.office@changing-lives.org.uk
                    </a>
                </nav>
            </div>
        )
    }
}

export default InfoBar
