import React from 'react';
import './video-viewing.styles.css';
import VideoService from "../../services/video-service";
import LoadingSpinner from "../shared/loading-spinner/loading-spinner.component";

class VideoViewing extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isFetching: false,
            videoId: null,
            video: null,
            downloadURL: null
        };
    }

    async componentDidMount(): void {

        await this.setState({...this.state, videoId: this.props.match.params.id});

        this.loadVideoInformation()
            .catch((error) => {

                alert(error.message);
            });
    }

    async loadVideoInformation(): Promise<void> {

        this.setState({...this.state, isFetching: true});

        let video = await VideoService.getVideo(this.state.videoId)
            .catch((error) => {

                this.setState({...this.state, isFetching: false});
                throw error;
            });

        let downloadURL = await VideoService.getDownloadURL(this.state.videoId)
            .catch((error) => {

                this.setState({...this.state, isFetching: false});
                throw error;
            });

        this.setState({...this.state, isFetching: false, video: video, downloadURL: downloadURL});
    }

    render() {

        return (

            <div>
                {this.state.isFetching &&
                <div className="text-center video-viewing-spinner-container">
                    <LoadingSpinner/>
                </div>
                }
                {!this.state.isFetching &&
                <div className="container-fluid video-container">
                    <div className={"row"}>
                        <div className={"col-3"}>
                        </div>
                        <div className="col-6">
                            <h1>
                                {this.state.video != null && this.state.video.title}
                            </h1>
                        </div>
                        <div className="col-3">
                        </div>
                    </div>
                    <div className={"row"}>
                        <div className={"col-3"}>
                        </div>
                        <div className="col-3">
                        </div>
                    </div>
                    <div className="row">
                        <div className={"col-3"}>
                        </div>
                        <div className="col-6">
                            <video width={"100%"} controls src={this.state.downloadURL}>
                                <source type="video/mp4">
                                </source>
                            </video>
                            <div className="col-6">
                                <h6>
                                    {this.state.video != null && this.state.video.description}
                                </h6>
                            </div>
                        </div>
                        <div className="col-3">
                        </div>
                    </div>
                </div>
                }
            </div>
        );
    }
}

export default VideoViewing;
