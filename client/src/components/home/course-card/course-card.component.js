import React from 'react';
import placeholderImage from "../../../Images/test.jpg";
import {Link} from "react-router-dom";
import "./course-card.styles.css";

class CourseCard extends React.Component {

    render() {

        return (
            <div className="card text-white">
                <img src={placeholderImage} className="card-img-top" alt="Loading"/>
                <div className="card-body" style = {{background: '#303572'}}>
                    <h5 className="card-title">
                        {this.props.title}
                    </h5>
                    <Link to={"/course/" + this.props.courseId}>
                        <button className="btn btn-outline-light" type="button">
                            View
                        </button>
                    </Link>
                </div>
            </div>
        )
    }
}

export default CourseCard;
