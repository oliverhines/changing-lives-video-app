import React from 'react';
import TitleImage from "../shared/title-image/title-image.component";
import OptionBar from "../shared/option-bar/option-bar.component";
import CourseCards from "./course-cards/course-cards.component";

class Home extends React.Component {
    render() {
        return (
            <div>
                <TitleImage />
                <OptionBar />
                <CourseCards />
            </div>
        );
    }
}

export default Home;
