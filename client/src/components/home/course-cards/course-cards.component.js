import React from 'react';
import CourseService from "../../../services/course-service";
import CourseCard from "../course-card/course-card.component";
import "./course-cards.styles.css";
import LoadingSpinner from "../../shared/loading-spinner/loading-spinner.component";

class CourseCards extends React.Component {

    state = {

        isFetching: false,
        courses: []
    };

    componentDidMount(): void {

        this.fetchCourses();
    }

    fetchCourses(): void {

        this.setState({...this.state, isFetching: true});

        CourseService.listCourses()
            .then((courses) => {

                this.setState({...this.state, isFetching: false, courses: courses});
            })
            .catch((error) => {

                this.setState({...this.state, isFetching: false});
                alert(error.message);
            });
    }

    render() {

        return (
            <div>
                <div className="nav justify-content-center">
                    {this.state.courses.map(course =>
                        <CourseCard title={course.name} courseId={course.id}/>

                    )}
                </div>
                {this.state.isFetching &&
                <div className="text-center course-cards-spinner-container">
                    <LoadingSpinner />
                </div>
                }
            </div>
        )
    }
}

export default CourseCards;
