import React from 'react';
import CourseService from "../../services/course-service";

class CourseCreate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: ""
        };

        this.updateName = this.updateName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        CourseService.createCourse(this.state.name)
            .then(() => {

                alert("Course successfully created");
            })
            .catch((error) => {

                alert(error.message);
            })
    }

    updateName(event) {

        this.setState({...this.state, name: event.target.value});
    }

    render() {

        return(
            <div>
                <h2 className="text-center">Create new course</h2>
                <hr/>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group row">
                                    <div className="col-9">
                                        <input type="text" className="form-control form-control-lg" id="nameInput"
                                               placeholder="Enter a name for your course..." value={this.state.name}
                                               onChange={this.updateName}/>
                                    </div>
                                    <div className="col-3">
                                        <button type="submit" className="btn btn-primary" style={{width: "100%", height: "100%"}}>
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default CourseCreate;
