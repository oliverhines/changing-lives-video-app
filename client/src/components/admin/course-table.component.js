import React from 'react';
import CourseService from "../../services/course-service";
import CourseRow from "./course-row";
import {Link} from "react-router-dom";

class CourseTable extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            courses: []
        };
    }

    componentDidMount(): void {

        CourseService.listCourses()
            .then((courses) => {

                this.setState({...this.state, courses: courses});
            })
            .catch((error) => {

                alert(error.message);
            });
    }

    render() {

        return(
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>
                                <h4>
                                    Courses
                                </h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.courses.map((course) => {
                            return(<CourseRow course={course}/>)
                        })}
                        <tr>
                            <th>
                                <Link to={"/admin/course/create"}>
                                    <button className="btn btn-primary">
                                        Add Course
                                    </button>
                                </Link>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default CourseTable;
