import React from 'react';
import CourseTable from "./course-table.component";

class Admin extends React.Component {

    render() {

        return(
            <div>
                <h2 className="text-center">Admin</h2>
                <hr/>
                <div className="container">
                    <div className="row">
                        <div className="col-sm">
                            <CourseTable/>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Admin;
