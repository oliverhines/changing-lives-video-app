import React from 'react';
import Home from "./components/home/home.component";
import InfoBar from "./components/shared/info-bar/info-bar.component";
import Header from "./components/shared/header/header.component";
import Footer from "./components/shared/footer/footer.component";
import CourseHome from "./components/course-home/course-home.component";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import UploadHome from "./components/upload/upload-home";
import VideoViewing from "./components/video-viewing/video-viewing.component";
import Admin from "./components/admin/admin.component";
import CourseCreate from "./components/admin/course-create.component";

class App extends React.Component {

  render() {
    return (
        <div>
            <Router>
                <InfoBar />
                <Header />
                <Switch>
                    <Route exact path={"/"} component={Home}>
                    </Route>
                    <Route exact path={"/course/:id"} component={CourseHome}/>
                    <Route exact path={"/upload"} component={UploadHome}/>
                    <Route exact path={"/video/:id"} component={VideoViewing}/>
                    <Route exact path={"/admin"} component={Admin}/>
                    <Route exact path={"/admin/course/create"} component={CourseCreate}/>
                </Switch>
                <Footer />
            </Router>
        </div>
  )};
}

export default App;
