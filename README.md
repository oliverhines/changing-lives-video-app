# Changing Lives Video App
This repository contains code used to run the Changing Lives video app.
It is used to display internal videos to employees of Changing Lives.

The application is split into three main parts. They are the API used to
marshall the retrieval of content from the frontend and backend, the client
which is a web application used to display content to the user and finally
a database that stores information in the app. There is also another aspect to
the application which is the video storage facility. However, this is
implemented using Google Cloud Storage and as such is separate from the
rest of the application and requires no configuration by the administrator.

## Installation
The application runs using Docker to ensure that there is interoperability
between different operating systems and hardware. Therefore, the only system
requirement to run the program is that Docker can be installed.

To install Docker, go to [https://docs.docker.com/install/](https://docs.docker.com/install/)
and follow the installation instructions.

After installing Docker, open Docker and make sure that it is successfully
running. It may be necessary for you to change some system settings,
in particular the ability to virtualise is often not enabled by
default.

From here navigate to the root directory of the project. Running the application should be as simple as running
the command `docker-compose up --build`. This will individually build the
database, API and client and run them on the ports 5432, 8080 and 3000
respectively.

You should now be able to run the client by navigating from your web browser
to [http://localhost:3000](http://localhost:3000).
